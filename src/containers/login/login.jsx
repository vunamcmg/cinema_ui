import * as React from "react";
import { connect } from "react-redux";
import { Grid, Col, Row, Panel, FormGroup, Button, Modal } from 'react-bootstrap'
import "./login.css";
import { Redirect } from 'react-router-dom'
import api from '../../services'
import { userAction } from '../../actions'
class Login extends React.Component {
  constructor(props) {
    super(props)
    this.handleSubmit = this.handleSubmit.bind(this);
    this.checkUserAlreadyLogin = this.checkUserAlreadyLogin.bind(this)
    this.hideLoginErrorModal = this.hideLoginErrorModal.bind(this)
    this.isLogin = false
    this.state = {
      showErrorModal: false,
      showRegisterComponent: false,
      isLogin: false,
      type: null,
      email: "",
      password: "",
    }
  }
  isLogin
  async hideLoginErrorModal() {
    this.setState({ showErrorModal: false })
  }
  async handleSubmit(event) {
    event.preventDefault();
    try {
      const result = await api.employee.getList({
        query: {
          fields: ["$all"], filter: { email: this.refs.email.value, password: this.refs.password.value }
        }
      })
      if (result.length == 0) {
        alert("Đăng nhập không thành công")
        return
      }
      await this.props.dispatch(userAction.login(result[0]))
      await localStorage.setItem("isLogin", true)
      await localStorage.setItem("type", result[0].type)
      await localStorage.setItem("id", result[0].id)
      this.forceUpdate()
      console.log("result: ", result)
    } catch (error) {
      alert("Đăng nhập không thành công")
      return
    };

  }

  async checkUserAlreadyLogin() {
    const isLogin = localStorage.getItem('isLogin')
    const type = localStorage.getItem('type')
    if (isLogin === "true") {
      this.setState({ isLogin: true, type })
    }
  }
  render() {
    this.checkUserAlreadyLogin()
    console.log("Login state: ", this.state)
    if (this.state.isLogin) {
      if (this.state.type === "MANAGER") {
        return <Redirect to="/manager" />
      } else {
        return <Redirect to="/booking" />
      }

    }
    return (
      <div className="login-wrapper">
        <div className="login-form">
          <form >
            <div className="form-group">
              <input
                type="text"
                className="form-control contact-input"
                placeholder="Email"
                ref="email"
                required
              />
            </div>
            <div className="form-group">
              <input
                type="password"
                className="form-control contact-input"
                placeholder="Mật khẩu"
                ref="password"
                required
              />
            </div>

          </form>
          <button onClick={this.handleSubmit} className="login-button">Đăng nhập</button>
        </div>
      </div>

    )
  }
}


const mapStateToProps = (state) => {
  return state;
};

export default connect(mapStateToProps)(Login);
