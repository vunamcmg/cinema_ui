import * as React from "react";

import { connect } from "react-redux";

import { Slide, HotFilm } from './components/index'
import api from '../../services/index'
import { filmAction, scheduleAction, roomAction } from '../../actions'
import * as Khongdau from 'khong-dau'
import * as moment from 'moment';

import {
  Header,

  Footer,
} from "../../components/index";
import './home.css'

class Home extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      films: [],
      selectedFilm: {
        avatar: "https://p.w3layouts.com/demos/cinema/web/images/header-bg.jpg"
      }
    }
    this.selectFilm = this.selectFilm.bind(this)
    this.onSearchChange = this.onSearchChange.bind(this)
  }
  onSearchChange() {
    if (this.refs.searchFilmQuery) {
        const query = Khongdau(this.refs.searchFilmQuery.value)
        const films = this.props.film.filter((film) => { return Khongdau(film.name).toLowerCase().indexOf(query) !== -1 })
        this.setState({ films: films })
    } else {
        this.setState({ films: this.props.film })
    }
}
  async selectFilm(id) {
    console.log("selected film: ", id)
    const selectedFilm = this.state.films.find(item => { return item.id == id })
    console.log("film: ", selectedFilm)
    this.setState({ selectedFilm: selectedFilm })
    window.scrollTo(0, 0)
    this.forceUpdate()
  }
  async componentDidMount() {
    try {
      if (this.props.film.length == 0) {
        let films = await api.film.getList({ query: { fields: ["$all", { "schedule_films": ["$all", { "room": ["$all"] }] }], limit: 1000 } })
        films = films.reverse()
        this.props.dispatch(filmAction.fetch(films))
        this.setState({ films: films, selectedFilm: films[0] })
        console.log("film : ", films[0])
      }
    } catch (error) {
      alert("Api error")
    }

  }
  render() {
    const { selectedFilm } = this.state
    const headerStyle = {
      background: `url(${selectedFilm.avatar}) no-repeat 0px 0px`,
      "background-attachment": "cover",
      minHeight: "750px",
      float: "left"
    }
    const { film } = this.props
    const films = this.state.films ? this.state.films : this.props.film
    return (
      <div>
        <div>
          <div class="header" >
            <div class="top-header">
              <div class="logo">
                <a href="index.html"><img src="images/logo.png" alt="" /></a>
                <p>Lotte cinema</p>
              </div>

              <div class="search">
                <form>
                  <input ref="searchFilmQuery" placeholder="Tìm kiếm" onChange={this.onSearchChange} className="contact-input search-film-input"/>
                </form>
              </div>
              <div class="clearfix"></div>
            </div>
            <hr/>
            <div style={headerStyle}></div>
            {selectedFilm ?
              <div class="header-info" >
                <h1>{selectedFilm.name}</h1>
                <p >Thời gian bắt đầu công chiếu: {moment(selectedFilm.start_time).format("DD-MM-YYYY")}</p>
                {/* <p class="age"><a href="#">All Age</a> Don Hall, Chris Williams</p> */}
                {/* <p class="review">Rating	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: &nbsp;&nbsp;  8,5/10</p> */}
                {/* <p class="review reviewgo">Genre	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : &nbsp;&nbsp; Animation, Action, Comedy</p> */}
                {/* <p class="review">Release &nbsp;&nbsp;&nbsp;&nbsp;: &nbsp;&nbsp; 7 November 2014</p> */}
                <p class="special" style={{ overflow: "auto", maxHeight: "300px", minHeight: "300px" }} dangerouslySetInnerHTML={{ __html: selectedFilm.description }} ></p>
                <a class="video" onClick={() => { window.open(selectedFilm.trailer, "_blank") }}><i class="video1"></i>TRAILER</a>
              </div>
              : null}
          </div>

        </div>
        <HotFilm films={films} selectFilm={this.selectFilm} />
        {/* <Footer /> */}
      </div >

    );
  }
}

const mapStateToProps = (state) => {
  return state;
};

export default connect(mapStateToProps)(Home);
