import * as React from "react";

import "./hotFilm.css";
class HotFilm extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    const { films } = this.props
    return (
      <div className="best-location">
        <div className="container-fluid">
          <div className="row best-location-row">
            {films.map(item => {
              return (
                <div
                  className="col-lg-4 col-md-4 col-xs-12 best-location-element"
                  id="item-1"
                  onClick={() => { return this.props.selectFilm(item.id)}}
                >
                  <img
                    src={item.avatar}
                    alt=""
                    className="img-responsive"
                  />
                </div>
              )
            })}
            {/* <div
              className="col-lg-4 col-md-4 col-xs-12 best-location-element"
              id="item-2"
            >
              <img
                src="https://p.w3layouts.com/demos/cinema/web/images/r1.jpg"
                alt=""
                className="img-responsive"
              />
            </div>
            <div
              className="col-lg-4 col-md-4 col-xs-12 best-location-element"
              id="item-3"
            >
              {" "}
              <img
                src="https://p.w3layouts.com/demos/cinema/web/images/r5.jpg"
                alt=""
                className="img-responsive"
              />
            </div>
          </div>
          <div className="row best-location-row">
            <div
              className="col-lg-4 col-md-4 col-xs-12 best-location-element"
              id="item-4"
            >
              {" "}
              <img
                src="https://p.w3layouts.com/demos/cinema/web/images/r6.jpg"
                alt=""
                className="img-responsive"
              />
            </div>
            <div
              className="col-lg-4 col-md-4 col-xs-12 best-location-element"
              id="item-5"
            >
              {" "}
              <img
                src="https://p.w3layouts.com/demos/cinema/web/images/r3.jpg"
                alt=""
                className="img-responsive"
              />
            </div>
            <div
              className="col-lg-4 col-md-4 col-xs-12 best-location-element"
              id="item-6"
            >
              {" "}
              <img
                src="https://p.w3layouts.com/demos/cinema/web/images/r4.jpg"
                alt=""
                className="img-responsive"
              />{" "}
            </div> */}
          </div>
        </div>
      </div>
    );
  }
}

export default HotFilm;
