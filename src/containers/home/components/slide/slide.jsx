import * as React from "react";

import "./slide.css";
class Slide extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div className="wrap">
        <div className="arrow" id="arrow-left">
          {" "}
          <p> &lt; </p>
        </div>
        <div id="slider">
          <div className="slide slider-1">
            <div className="slider-content">
              <p>
                <u>Đà Lạt</u>
              </p>
              <span>ĐỒI THÔNG ĐÀ LẠT</span>
            </div>
          </div>
          {/*<div className="slider-2">
          <img src="https://i.imgur.com/IXLcGZY.jpg" />{" "}
        </div>
        <div className="slider-3">
          <img src="https://i.imgur.com/IXLcGZY.jpg" />{" "}
        </div>*/}
        </div>
        <div className="arrow" id="arrow-right">
          <p> &gt; </p>
        </div>
      </div>
    );
  }
}

export default Slide;
