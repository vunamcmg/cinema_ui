import * as React from "react";
import api from '../../services/index'
import { connect } from "react-redux";
import {
  Tooltip,
} from 'react-tippy';

import { Redirect } from 'react-router-dom'
import {
  Sidebar
} from "../../components/index";
import './booking.css'

import { Films, Seats, Form, Customer, BillInfo, CustomerDetail } from './components/index'

import { TextInput, SelectInput } from '../../elements/index'
import { filmAction, customerAction, userAction } from '../../actions'

class Booking extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      components: [{
        code: "films", isShow: true
      }, {
        code: "form", isShow: false
      }, {
        code: "seats", isShow: false
      }, {
        code: "success", isShow: false
      }, {
        code: "customer", isShow: false
      }, {
        code: "bill_info", isShow: false
      }, {
        code: "customer_info", isShow: false
      }],
      selectedFilm: null,
      ticketAmount: 1,
      scheduleFilm: null,
      scheduleFilmId: null,
      scheduleFilmInfo: null,
      billId: null,
      customerId: null,
      isReservate: false,
      isChangeTickets: false,
      isLogin: true
    }
    this.checkUserAlreadyLogin = this.checkUserAlreadyLogin.bind(this)
    this.openChildComponent = this.openChildComponent.bind(this)
    this.renderChild = this.renderChild.bind(this)
    this.selectFilm = this.selectFilm.bind(this)
    this.submitInfo = this.submitInfo.bind(this)
    this.onSearchChange = this.onSearchChange.bind(this)
    this.order = this.order.bind(this)
    this.openCustomerDetail = this.openCustomerDetail.bind(this)
    this.customerOrder = this.customerOrder.bind(this)
    this.customerReservate = this.customerReservate.bind(this)
    this.createCustomer = this.createCustomer.bind(this)
    this.openFilmComponent = this.openFilmComponent.bind(this)
    this.changeTicket = this.changeTicket.bind(this)
    this.logout = this.logout.bind(this)
  }
  async logout() {
    await localStorage.setItem("isLogin", false)
    await localStorage.setItem("type", null)
    await localStorage.setItem("id", null)
    this.props.dispatch(userAction.logout())
    this.setState({ isLogin: false })
}
  async changeTicket(billId, customerId) {
    await this.setState({ customerId: customerId, billId: billId, isChangeTickets: true })
    this.openChildComponent("seats")
  }
  openFilmComponent() {
    this.setState({ customerId: null })
    this.openChildComponent("films")
  }
  async createCustomer(body) {

    try {
      const result = await api.customer.add(body)
      this.props.dispatch(customerAction.add({
        id: result.id,
        name: body.name,
        phone: body.phone,
        birthday: body.birthday
      }))
      this.setState({ customerId: result.id })
      alert("Tạo khách hàng thành công")
    } catch (error) {
      console.log('err: ', error)
      alert("Tạo khách hàng không thành công")
    }
  }
  async customerReservate(customerId) {
    this.setState({ customerId: customerId, isReservate: true })
    this.openChildComponent("films")
  }
  async customerOrder(customerId) {
    this.setState({ customerId: customerId })
    this.openChildComponent("films")
  }
  async openCustomerDetail(customerId) {
    this.setState({ customerId: customerId })
    this.openChildComponent("customer_info")
  }
  async order(tickets) {
    let employee_id = this.props.user.id
    if(!employee_id){
      employee_id = localStorage.getItem("id")
    }
    const body = {
      schedule_film_id: this.state.scheduleFilmInfo.id,
      tickets: tickets,
      customer_id: this.state.customerId,
      employee_id: employee_id,
      note: "Mua vé"
    }
    try {
      let result = {}
      if (this.state.isReservate) {
        result = await api.bill.order(body)
      } else {
        result = await api.bill.buyTicket(body)
      }
      this.setState({ billId: result.id })
      alert("Đặt vé thành công")
      this.openChildComponent("bill_info")
    } catch (err) {
      console.log("errL ", err)
      alert("Đặt vé không thành công")
    }
  }
  onSearchChange(query) {
    this.props.dispatch(filmAction.search(query.toLowerCase()))
  }
  async submitInfo(info) {
    this.setState(info)
    this.openChildComponent("seats")
  }
  async selectFilm(id) {
    const film = this.props.film.find((item) => { return item.id === id })
    if (film.schedule_films.length === 0) {
      alert("Phim này chưa có lịch chiếu nào, vui lòng chọn phim khác")
      return
    }
    await this.setState({
      selectedFilm: film
    })
    setTimeout(this.openChildComponent("form"), 1000)

  }
  async openChildComponent(code) {
    if (code === "form" && this.state.selectedFilm === null) {
      alert("Vui lòng chọn phim")
      return
    }
    if (code === "seats" && this.state.selectedFilm === null && this.state.isChangeTickets === false) {
      alert("Vui lòng chọn phim")
      return
    }
    let newComponent = []
    for (const component of this.state.components) {
      if (component.code === code) {
        component.isShow = true
      } else {
        component.isShow = false
      }
      newComponent.push(component)
    }
    this.setState({ component: newComponent })
  }
  renderChild() {
    for (const component of this.state.components) {
      if (component.isShow) {
        switch (component.code) {
          case "seats":
            return <Seats  {...this.props} {...this.state} order={this.order} />
          case "films":
            return <Films {...this.props}  {...this.state} selectFilm={this.selectFilm} onSearchChange={this.onSearchChange} />
          case "form":
            return <Form {...this.props} {...this.state} submitInfo={this.submitInfo} createCustomer={this.createCustomer} />
          case "customer":
            return <Customer {...this.props} openCustomerDetail={this.openCustomerDetail} customerOrder={this.customerOrder} customerReservate={this.customerReservate} />
          case "bill_info":
            return <BillInfo {...this.props} {...this.state} />
          case "customer_info":
            return <CustomerDetail {...this.props} {...this.state} customerOrder={this.customerOrder} customerReservate={this.customerReservate} changeTicket={this.changeTicket} />
        }
      }
    }

  }
  async componentWillMount() {
    try {
      if (this.props.film.length == 0) {
        let films = await api.film.getList({ query: { fields: ["$all", { "schedule_films": ["$all"] }], limit: 1000 } })
        films = films.reverse()
        await this.props.dispatch(filmAction.fetch(films))
      }
    } catch (error) {
      alert("Lỗi mạng")
    }
  }
  async componentDidMount() {
    // try {
    //   if (this.props.film.length == 0) {
    //     let films = await api.film.getList({ query: { fields: ["$all", { "schedule_films": ["$all"] }], limit: 1000 } })
    //     films = films.reverse()
    //     this.props.dispatch(filmAction.fetch(films))
    //   }
    // } catch (error) {
    //   alert("Lỗi mạng")
    // }
  }

  componentWillReceiveProps(nextProps) { }

  shouldComponentUpdate(nextProps, nextState) {
    return true;
  }
  async checkUserAlreadyLogin() {
    const isLogin = localStorage.getItem('isLogin')
    if (isLogin !== "true") {
      this.setState({ isLogin: false })
    }
  }
  render() {
    this.checkUserAlreadyLogin()
    if (!this.state.isLogin) {
      return <Redirect to="/login" />
    }
    const ChildComponent = this.renderChild()

    return (
      <div>

        <div className="admin-navbar">
          <h1 className="admin-nav-item admin-nav-brand">Lotte Cinema Manager</h1>
          <div className="admin-navbar-right">
            <Tooltip
              title="Thông báo"
              position="top"
            >
              <button className="admin-navbar-button"><i class="far fa-bell"></i></button>
            </Tooltip>
            <Tooltip
              title="Thư"
              position="top"
            >
              <button className="admin-navbar-button"><i class="far fa-envelope"></i></button>
            </Tooltip>
            <Tooltip
              title="Bình luận"
              position="top"
            >
              <button className="admin-navbar-button"><i class="far fa-comment"></i></button>
            </Tooltip>
            <Tooltip
              title="Đăng xuất"
              position="top"
            >
              <button onClick={this.logout} className="admin-navbar-button"><i class="fas fa-sign-out-alt"></i></button>
            </Tooltip>
          </div>
          <div>

          </div>
        </div>
        <div className="admin-main">

          <Sidebar>
            <ul>
              {this.props.user.type === "MANAGER" ? <li className="admin-sidebar-link" onClick={() => { return this.props.history.push("/manager") }}> Tổng quan </li> : null}
              <li className="admin-sidebar-link" onClick={() => this.openFilmComponent()}> Phim </li>
              <li className="admin-sidebar-link" onClick={() => this.openChildComponent("customer")}> Khách hàng </li>
              {/* <li className="admin-sidebar-link" onClick={() => this.openChildComponent("ticket")}> Cập nhật đặt chỗ </li> */}
            </ul>
          </Sidebar>
          <div className="admin-block">
            {this.state.components[0].isShow || this.state.components[1].isShow || this.state.components[2].isShow ? <div className="booking-stepper">
              <div>
                <button className={this.state.components[0].isShow ? "step-active" : null} onClick={() => this.openChildComponent("films")}>1: Chọn phim</button>
              </div>
              <div className="step-line"></div>
              <div>
                <button className={this.state.components[1].isShow ? "step-active" : null} onClick={() => this.openChildComponent("form")}>2: Điền thông tin</button>
              </div>
              <div className="step-line"></div>
              <div>
                <button className={this.state.components[2].isShow ? "step-active" : null} onClick={() => this.openChildComponent("seats")}>3: Chọn chỗ ngồi</button>
              </div>
            </div> : null}
            <div className="block-wrapper">
              {ChildComponent}
            </div>
          </div>

        </div>

      </div >
    );
  }
}

const mapStateToProps = (state) => {
  return state;
};

export default connect(mapStateToProps)(Booking);
