import Films from './films/films'
import Seats from './seats/seats'
import Form from './form/form'
import Customer from './customer/customer'
import BillInfo from './billInfo/billInfo'
import CustomerDetail from './customerDetail/customerDetail'

export {
    Films, Seats, Form, Customer, BillInfo, CustomerDetail
}