import * as React from "react";
import {
    Tooltip,
} from 'react-tippy';
import DatePicker from "react-datepicker";
import * as moment from 'moment'
import * as Khongdau from 'khong-dau'
import { SelectInput } from '../../../../elements'


import "./films.css";
class Films extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            films: undefined,
            start_time: moment().format(),
            filmStatus:null
        }
        this.open = this.open.bind(this)
        this.onSearchChange = this.onSearchChange.bind(this)
        this.handleChange = this.handleChange.bind(this)
        this.handleSearchStatusChange = this.handleSearchStatusChange.bind(this)
    }
    handleSearchStatusChange(field, value){
        this.setState({
            filmStatus: value
        })
        
        if (value && value !== "ALL") {
            const films = this.props.film.filter((film) => { return film.status === value })
            this.setState({ films: films })
        } else {
            this.setState({ films: this.props.film })
        }
    }
    handleChange(date) {
        this.setState({
            start_time: moment(date).format()
        });
        if (this.refs.searchFilmQuery) {
            const films = this.props.film.filter((film) => { return moment(date).startOf('day').isSame(moment(film.start_time).startOf('day')) })
            this.setState({ films: films })
        } else {
            this.setState({ films: this.props.film })
        }
    }
    async open(trailer) {
        window.open(trailer, "_blank")
    }
    async componentDidMount() {
        // this.setState({ films: this.props.film })
        // this.forceUpdate()
    }

    componentWillReceiveProps(nextProps) {

        return true
    }

    shouldComponentUpdate(nextProps, nextState) {
        return true;
    }
    onSearchChange() {
        if (this.refs.searchFilmQuery) {
            const query = Khongdau(this.refs.searchFilmQuery.value)
            const films = this.props.film.filter((film) => { return Khongdau(film.name).toLowerCase().indexOf(query) !== -1 })
            this.setState({ films: films })
        } else {
            this.setState({ films: this.props.film })
        }
    }
    render() {
        // const { films } = this.state
        const { film } = this.props
        const films = this.state.films ? this.state.films : this.props.film
        // const films = this.onSearchChange()
        const filmStatus = [
            {
                id: "WILL_BE_RELEASE",
                name:"Sắp chiếu"
            },
            {
                id: "RELEASING",
                name:"Đang chiếu"
            },
            {
                id: "RELEASED",
                name:"Dừng công chiếu"
            },
            {
                id: "ALL",
                name: "Tất cả"
            }
        ]
        return (
            <div className="posts">
                <div className="search-film">
                    <div>
                    <label>Tên phim</label>
                    <br/>
                    <input ref="searchFilmQuery" onChange={this.onSearchChange} className="contact-input search-film-input"/>
                    </div>

                    <div className="form-group search-film-date" >
                        <label>Ngày khởi chiếu</label>
                        <br/>
                        <DatePicker
                            className="contact-input"
                            selected={this.state.start_time}
                            onChange={this.handleChange}
                           
                            timeIntervals={15}
                            dateFormat="d/MM/yyyy"
                            timeCaption="time"
                        />
                    </div>
                    <div>
                    <SelectInput label="Tình trạng" refKey="filmStatus" data={filmStatus} titleKey="name" valueKey="id" handleInputChange={this.handleSearchStatusChange} />
                    </div>
                </div>
                <table className="admin-posts-table">
                    <tbody>
                        <tr>
                            <th>Thứ tự</th>
                            <th>Poster</th>
                            <th>Tên phim</th>
                            <th>Ngày khởi chiếu</th>
                            <th>Tình trạng</th>
                            <th>Thao tác</th>
                        </tr>
                        {films.map((item, index) => {
                            let status
                            switch(item.status){
                                case "WILL_BE_RELEASE":
                                    status = "Sắp công chiếu"
                                    break
                                case "RELEASING":
                                    status = "Đang công chiếu"
                                    break
                                case "RELEASED":
                                    status = "Đã công chiếu"
                                    break
                                default:
                                    status = "Đang cập nhật"
                            }
                            return (
                                <tr key={item.id} className={this.props.selectedFilm && this.props.selectedFilm.id === item.id ? "selected-film" : null} onClick={() => this.props.selectFilm(item.id)}>
                                    <td>{index + 1}</td>
                                    <td ><img src={item.avatar} className="img-response avatar-image"></img></td>
                                    <td>{item.name}</td>
                                    <td>{moment(item.start_time).format('DD/MM/YYYY')}</td>
                                    <td>{status}</td>
                                    <td className="action-td">
                                        <Tooltip
                                            title="Xem trailer"
                                            position="top"
                                        >
                                            <span className="post-open-button" onClick={() => this.open(item.trailer)}><i class="fas fa-share-square"></i></span>
                                        </Tooltip>
                                        <Tooltip
                                            title="Chọn phim"
                                            position="top"
                                        >
                                            <span className="post-edit-button" onClick={() => this.props.selectFilm(item.id)}> <i class="fas fa-pen"></i> </span>
                                        </Tooltip>
                                    </td>
                                </tr>

                            )
                        })}
                    </tbody>
                </table>
            </div>
        )
    }
}

export default Films