import * as React from "react";
import api from '../../../../services'
import "./customerDetail.css";
import {
    Tooltip,
} from 'react-tippy';
import * as moment from 'moment'
import { customerAction } from '../../../../actions'

class CustomerDetail extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            customerInfo: null
        }
        this.handleInputChange = this.handleInputChange.bind(this)
        this.update = this.update.bind(this)
        this.paybill = this.paybill.bind(this)
        this.cancelTicket = this.cancelTicket.bind(this)
    }
    async paybill(id, index) {
        try {
            const result = await api.bill.paybill(id)
            this.state.customerInfo.bills[index] = result
            this.setState({ customerInfo: this.state.customerInfo })
            alert("Thanh toán thành công")
        } catch (err) {
            alert(err)
        }
    }
    async cancelTicket(id, index){
        console.log("index: ", index)
        console.log("id: ", id)
        try {
            const result = await api.bill.cancelTicket(id)
            this.state.customerInfo.bills[index] = result
            this.setState({ customerInfo: this.state.customerInfo })
            this.setState({ customerInfo: this.state.customerInfo })
            alert("Huỷ đặt chỗ thành công")
        } catch (err) {
            alert(err)
        }
    }
    async update(event) {
        event.preventDefault();
        try {
            const body = {
                name: this.refs.name.value,
                phone: this.refs.phone.value,
                birthday: this.refs.birthday.value
            }

            const result = await api.customer.update(this.props.customerId, body)
            this.props.dispatch(customerAction.update({
                id: this.state.customerId,
                name: this.refs.name.value,
                phone: this.refs.phone.value,
                birthday: this.refs.birthday.value
            }))
            alert("Cập nhật thành công")
        } catch (error) {
            console.log('err: ', error)
            alert("Cập nhật không thành công")
        }
    }
    handleInputChange() {

    }
    async componentDidMount() {
        try {
            const customerInfo = await api.customer.getItem(
                this.props.customerId,
                {
                    query: {
                        fields: ["$all", { "bills": ["$all"] }]
                    }
                })
            this.setState({ customerInfo: customerInfo })
        } catch (err) {
            alert("Lỗi")
        }
    }
    render() {
        const { customerInfo } = this.state
        return (
            <div className="customerDetail">
                {this.state.customerInfo ? (
                    <div>
                        <form onSubmit={this.update} id="add-post-form" encType="multipart/form-data">
                            <div className="form-group">
                                <input
                                    type="text"
                                    className="form-control contact-input"
                                    placeholder="Họ tên"
                                    ref="name"
                                    defaultValue={customerInfo.name}
                                    required
                                />
                            </div>
                            <div className="form-group">
                                <input
                                    type="text"
                                    className="form-control contact-input"
                                    placeholder="Số điện thoại"
                                    ref="phone"
                                    defaultValue={customerInfo.phone}
                                    required
                                />
                            </div>
                            <div className="form-group">
                                <input
                                    type="text"
                                    className="form-control contact-input"
                                    placeholder="Ngày sinh"
                                    ref="birthday"
                                    defaultValue={customerInfo.birthday}
                                    required
                                />
                            </div>
                            <button type="submit" className="btn btn-primary" style={{ width: "150px", "margin-top": "14px" }}>
                                Cập nhật
                        </button>

                        </form>
                        <div className="section-header">
                            <h4>Danh sách hoá đơn</h4>
                            <Tooltip
                                title="Đặt vé"
                                position="top"
                            >
                                <i className="fa fa-plus" onClick={() => this.props.customerOrder(customerInfo.id)}></i>
                            </Tooltip>

                        </div>
                        <hr />
                        <div>
                            <table className="admin-posts-table">
                                <tbody>
                                    <tr>
                                        <th>Thứ tự</th>
                                        <th>Ngày đặt</th>
                                        <th>Tổng tiền</th>
                                        <th>Tình trạng</th>
                                        <th>Ghi chú</th>
                                        <th>Thao tác</th>
                                    </tr>
                                    {customerInfo.bills.map((item, index) => {
                                        let status
                                        switch (item.status) {
                                            case "PAID":
                                                status = "Đã thanh toán"
                                                break
                                            case "CANCEL":
                                                status = "Đã huỷ"
                                                break
                                            default:
                                                status = "Chưa thanh toán"
                                        }
                                        return (
                                            <tr key={item.id} >
                                                <td>{index + 1}</td>
                                                <td >{moment(item.created_at).format('DD/MM/YYYY HH:mm')}</td>
                                                <td>{item.total_price}</td>
                                                <td>{status}</td>
                                                <td>{item.note}</td>
                                                <td className="action-td">
                                                    {/* <Tooltip
                                                        title="Chỉnh sửa"
                                                        position="top"
                                                    >
                                                        <span className="post-edit-button" onClick={() => this.props.selectFilm(item.id)}> <i class="fas fa-pen"></i> </span>
                                                    </Tooltip> */}
                                                    {item.status == "UNPAID" ?
                                                        <div>
                                                            <Tooltip
                                                                title="Thanh toán"
                                                                position="top"
                                                            >
                                                                <span className="post-edit-button" onClick={() => this.paybill(item.id, index)}> <i class="far fa-credit-card"></i> </span>
                                                            </Tooltip>
                                                            <Tooltip
                                                                title="Đổi ghế"
                                                                position="top"
                                                            >
                                                                <span className="post-edit-button" onClick={() => this.props.changeTicket(item.id, this.state.customerInfo.id)}> <i class="fas fa-pen"></i> </span>
                                                            </Tooltip>
                                                            <Tooltip
                                                                title="Huỷ đặt chỗ"
                                                                position="top"
                                                            >
                                                                <span className="post-edit-button" onClick={() => this.cancelTicket(item.id, index)}> <i class="far fa-calendar-times"></i> </span>
                                                            </Tooltip></div> : null}
                                                </td>
                                            </tr>
                                        )
                                    })}
                                </tbody>
                            </table>
                        </div>
                    </div>
                )
                    : null}
            </div>

        )
    }
}

export default CustomerDetail