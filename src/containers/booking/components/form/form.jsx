import * as React from "react";
import {
    Tooltip,
} from 'react-tippy';
import api from '../../../../services'

import { TextInput, SelectInput } from '../../../../elements/index'

import "./form.css";
class Form extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            ticketAmount: 1,
            scheduleFilm: null,
            customerName: null,
            customerPhone: null,
            customerBirthday: null,
            scheduleFilmInfo: null,
            ticketEmpty: null,
            ticketSold: null,
            isRegisCustomer: false
        }
        this.submitInfo = this.submitInfo.bind(this)
        this.handleInputChange = this.handleInputChange.bind(this)
        this.handleRegisCustomerChange = this.handleRegisCustomerChange.bind(this)
    }
    handleRegisCustomerChange(event) {
        this.setState({ isRegisCustomer: !this.state.isRegisCustomer })
    }
    async submitInfo() {
        if (this.state.isRegisCustomer) {
            const body = {
                name: this.state.customerName,
                phone: this.state.customerPhone,
                birthday: this.state.customerBirthday,
            }
            await this.props.createCustomer(body)
        }
        this.props.submitInfo(this.state)
    }
    async handleInputChange(field, value) {
        this.setState({ [field]: value })
        if (field === "scheduleFilm") {
            const scheduleFilmInfo = await api.scheduleFilm.getItem(value,
                {
                    query:
                        {
                            fields: ["$all", { "room": ["$all"] }, { "tickets": ["$all", { "seat": ["code"] }] }]
                        }
                })
            const ticketEmpty = scheduleFilmInfo.tickets.filter(item => { return item.status === "EMPTY" }).length
            const ticketSold = scheduleFilmInfo.tickets.filter(item => { return item.status === "SOLD" }).length
            await this.setState({ scheduleFilmInfo: scheduleFilmInfo, ticketEmpty: ticketEmpty, ticketSold: ticketSold })
        }
    }
    async componentDidMount() {
        const scheduleFilm = this.props.selectedFilm.schedule_films[0].id
        const scheduleFilmInfo = await api.scheduleFilm.getItem(scheduleFilm,
            {
                query:
                    {
                        fields: ["$all", { "room": ["$all"] }, { "tickets": ["$all", { "seat": ["code"] }] }]
                    }
            })
        const ticketEmpty = scheduleFilmInfo.tickets.filter(item => { return item.status === "EMPTY" }).length
        const ticketSold = scheduleFilmInfo.tickets.filter(item => { return item.status === "SOLD" }).length
        await this.setState({ scheduleFilm: scheduleFilm, scheduleFilmInfo: scheduleFilmInfo, ticketEmpty: ticketEmpty, ticketSold: ticketSold })
    }


    componentWillReceiveProps(nextProps) { }

    shouldComponentUpdate(nextProps, nextState) {
        return true;
    }
    render() {
        const { selectedFilm } = this.props
        return (
            <div className="posts">
                <form>
                    <TextInput label="Phim" refKey="film" placeholder="Mission impossible" value={selectedFilm.name} style={{}} readOnly={true} />
                    {/* <TextInput label="Số lượng vé" refKey="ticketAmount" placeholder="Số lượng vé" value="1" style={{}} handleInputChange={this.handleInputChange} /> */}
                    <SelectInput label="Suất chiếu" refKey="scheduleFilm" data={selectedFilm.schedule_films} titleKey="start_time" valueKey="id" handleInputChange={this.handleInputChange} time={true} />
                    <div className="schedule-info">
                        <TextInput label="Phòng" refKey="room" placeholder="Tên phòng chiếu" value={this.state.scheduleFilmInfo && this.state.scheduleFilmInfo.room? this.state.scheduleFilmInfo.room.name : null} style={{}} readOnly={true} />
                        <TextInput label="Loại phòng chiếu" refKey="roomType" placeholder="Loại phòng chiếu" value={this.state.scheduleFilmInfo && this.state.scheduleFilmInfo.room ? this.state.scheduleFilmInfo.room.type : null} style={{}} readOnly={true} />
                        <TextInput label="Số ghế trống" refKey="ticketEmpty" placeholder="Số ghế trống" value={this.state.ticketEmpty} style={{}} readOnly={true} />
                        <TextInput label="Số ghế đã bán" refKey="ticketEmpty" placeholder="Số ghế đã bán" value={this.state.ticketSold} style={{}} readOnly={true} />
                        {this.state.ticketEmpty === 0 ?
                            <p style={{ color: "red" }}>Vé đã bán hết</p> : null}
                        <TextInput label="Giá vé bình thường" refKey="ticketPrice" placeholder="Giá vé bình thường" value={this.state.scheduleFilmInfo ? this.state.scheduleFilmInfo.price : null} style={{}} readOnly={true} />
                        <TextInput label="Giá vé cho thành viên" refKey="ticketMemberPrice" placeholder="Giá vé cho thành viên" value={this.state.scheduleFilmInfo ? this.state.scheduleFilmInfo.price_member : null} style={{}} readOnly={true} />
                    </div>
                    {/* <TextInput label="Mã khách hàng" refKey="customerID" placeholder="C00001" value="" style={{}} handleInputChange={this.handleInputChange} /> */}
                    {!this.props.customerId ? <div><input type="checkbox" ref="regisCustomer" value="true" onChange={this.handleRegisCustomerChange} />Đăng ký thành viên<br /></div> : null}
                    {this.state.isRegisCustomer ?
                        <div>
                            <TextInput label="Họ và tên" refKey="customerName" placeholder="Lucas Vũ" value="" style={{}} handleInputChange={this.handleInputChange} />
                            <TextInput label="Số điện thoại" refKey="customerPhone" placeholder="0999999999" value="" style={{}} handleInputChange={this.handleInputChange} />
                            <TextInput label="Ngày sinh" refKey="customerBirthday" placeholder="01/01/2000" value="" style={{}} handleInputChange={this.handleInputChange} />
                        </div>
                        : null}
                </form>
                {this.state.ticketEmpty !== 0 ? <button onClick={this.submitInfo}>Chọn chỗ ngồi</button> : null}

            </div>
        )
    }
}

export default Form