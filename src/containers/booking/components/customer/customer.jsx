import * as React from "react";
import { TextInput, SelectInput } from '../../../../elements/index'
import "./customer.css";
import api from '../../../../services'
import { customerAction } from '../../../../actions'
import {
    Tooltip,
} from 'react-tippy';
class Customer extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            customers: [],
            selectedCustomer: null
        }
        this.handleInputChange = this.handleInputChange.bind(this)
        this.selectCustomer = this.selectCustomer.bind(this)
    }
    async selectCustomer(customer) {
        this.setState({ selectCustomer: customer })
    }
    async handleInputChange(field, value) {
        const customers = await api.customer.getList({ query: { fields: ["$all", { "bills": ["$all"] }], filter: { "phone": value } } })
        this.setState({ customers: customers })
    }
    async componentDidMount() {

    }
    render() {

        const { customers } = this.state
        return (
            <div className="posts">
                <form>
                    <TextInput label="Nhập số điện thoại khách hàng" refKey="customerPhone" placeholder="Số điện thoại khách hàng" value="" style={{}} handleInputChange={this.handleInputChange} />
                </form>
                <table className="admin-posts-table">
                    <tbody>
                        <tr>
                            <th>Thứ tự</th>
                            <th>Họ tên</th>
                            <th>ID</th>
                            <th>Ngày sinh</th>
                            <th>Số điện thoại</th>
                            <th>Thao tác</th>
                        </tr>
                        {customers.map((item, index) => {
                            return (
                                <tr key={item.id} className={this.state.selectedCustomer && this.state.selectedCustomer.id === item.id ? "selected-film" : null} onClick={() => this.selectCustomer(item)}>
                                    <td>{index + 1}</td>
                                    <td >{item.name}</td>
                                    <td>{item.id_card}</td>
                                    <td>{item.birthday}</td>
                                    <td>{item.phone}</td>
                                    <td className="action-td">
                                        <Tooltip
                                            title="Đặt vé"
                                            position="top"
                                        >
                                            <span className="post-open-button" onClick={() => this.props.customerOrder(item.id)}><i class="fas fa-ticket-alt"></i></span>
                                        </Tooltip>
                                        <Tooltip
                                            title="Đặt chỗ"
                                            position="top"
                                        >
                                            <span className="post-open-button" onClick={() => this.props.customerReservate(item.id)}><i class="fas fa-book"></i></span>
                                        </Tooltip>
                                        <Tooltip
                                            title="Chi tiết"
                                            position="top"
                                        >
                                            <span className="post-edit-button" onClick={() => this.props.openCustomerDetail(item.id)}> <i class="fas fa-pen"></i> </span>
                                        </Tooltip>
                                    </td>
                                </tr>

                            )
                        })}
                    </tbody>
                </table>
            </div>

        )
    }
}

export default Customer