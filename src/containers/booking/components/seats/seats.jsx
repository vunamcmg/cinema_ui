import * as React from "react";
import * as _ from 'lodash'
import "./seats.css";
import api from '../../../../services'
class Seats extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            selectedTickets: [],
            restTicketAmount: null,
            seatArr: [[]]
        }
        this.handleInputChange = this.handleInputChange.bind(this)
        this.selectTicket = this.selectTicket.bind(this)
        this.order = this.order.bind(this)
        this.changeTickets = this.changeTickets.bind(this)
    }
    async changeTickets() {
        try {
            const tickets = this.state.selectedTickets.map((item) => { return item.id })
            const body = {
                bill_id: this.props.billId,
                tickets: tickets
            }
            const result = await api.bill.changeTicket(body)
            alert("Thay đổi chỗ thành công")
        } catch (err) {
            alert(err)
        }
    }
    async order() {
        if (this.state.selectedTickets.length === 0) {
            alert("Chưa lựa chọn vé nào")
            return
        }
        const tickets = this.state.selectedTickets.map((item) => { return item.id })
        return this.props.order(tickets)
    }
    async selectTicket(ticket) {
        const selectedTickets = this.state.selectedTickets
        const selectedIndex = selectedTickets.findIndex(item => { return item.seat.code === ticket.seat.code })
        if (selectedIndex == -1) {
            selectedTickets.push(ticket)

        } else {
            selectedTickets.splice(selectedIndex, 1)
        }
        this.setState({ selectedTickets: selectedTickets })
    }
    handleInputChange() {

    }
    async componentDidMount() {
        if (this.props.isChangeTickets) {
            const bill = await api.bill.getItem(
                this.props.billId,
                {
                    query: {
                        fields: ["$all", { "customer": ["$all"] }, { "employee": ["$all"] }, { "bill_items": ["$all", { "ticket": ["$all", { "schedule_film": ["film_id", { "film": ["name"] }] }, { "seat": ["code"] }] }] }]
                    }
                })
            const scheduleFilmId = bill.bill_items[0].ticket.schedule_film_id
            const ticketIds = bill.bill_items.map(item => {
                return item.ticket_id
            })
            const scheduleFilmInfo = await api.scheduleFilm.getItem(scheduleFilmId,
                {
                    query:
                        {
                            fields: ["$all", { "room": ["$all"] }, { "tickets": ["$all", { "seat": ["code"] }] }]
                        }
                })
            let selectedTickets = []
            scheduleFilmInfo.tickets = scheduleFilmInfo.tickets.map(item => {
                if (ticketIds.indexOf(item.id) !== -1) {
                    item.status = "CURRENT"
                    selectedTickets.push(item)
                    return item
                } else {
                    return item
                }
            })
            this.setState({ selectedTickets: selectedTickets })
            const seatArr = _.chunk(scheduleFilmInfo.tickets, 10)
            await this.setState({ seatArr: seatArr })
        } else {
            this.setState({ restTicketAmount: this.props.ticketAmount })
            const seatArr = _.chunk(this.props.scheduleFilmInfo.tickets, 10)

            await this.setState({ seatArr: seatArr })
        }

    }
    render() {
        return (
            <div>
                <div className="choose-seat-info">
                    <div>Đã chọn: {this.state.selectedTickets.length}</div>
                    {/* <div>Còn lại: {this.props.ticketAmount - this.state.selectedTickets.length}</div> */}
                </div>
                <div className="seat-layout">

                    <div className="">
                        {
                            this.state.seatArr.map((childArr, index) => {
                                return (<div className="seat-row">
                                    {childArr.map((item, index) => {
                                        if (item.status === "EMPTY") {
                                            if (item.seat && item.seat.code) return <button onClick={() => { this.selectTicket(item) }} className={this.state.selectedTickets.findIndex(ticket => { return ticket.seat.code === item.seat.code }) !== -1 ? "selected-seat" : null} >{item.seat.code}</button>
                                        } else if (item.status === "CURRENT") {
                                            if (item.seat && item.seat.code) return <button onClick={() => { this.selectTicket(item) }} className={this.state.selectedTickets.findIndex(ticket => { return ticket.seat.code === item.seat.code }) !== -1 ? "selected-seat" : null}>{item.seat.code}</button>
                                        } else {
                                            return <button className="seat-reserved">{item.seat ? item.seat.code : null}</button>
                                        }
                                    })}
                                </div>)
                            })
                        }
                    </div>
                </div>
                <div className="order-button-wrapper">
                    {this.props.isChangeTickets ?
                        <div className="order-button">
                            <button onClick={this.changeTickets}>Đặt vé</button>
                        </div> :
                        <div className="order-button">
                            <button onClick={this.order}>Đặt vé</button>
                        </div>}
                </div>
            </div>

        )
    }
}

export default Seats