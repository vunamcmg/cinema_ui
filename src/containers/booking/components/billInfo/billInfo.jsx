import * as React from "react";
import api from '../../../../services'
import "./billInfo.css";
class BillInfo extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            billId: null,
            billInfo: null
        }
        this.handleInputChange = this.handleInputChange.bind(this)
    }

    handleInputChange() {

    }
    async componentDidMount() {

        try {
            const bill = await api.bill.getItem(
                this.props.billId,
                {
                    query: {
                        fields: ["$all", { "customer": ["$all"] }, { "employee": ["$all"] }, { "bill_items": ["$all", { "ticket": ["$all", { "schedule_film": ["film_id", { "film": ["name"] }] }, { "seat": ["code"] }] }] }]
                    }
                })
            this.setState({ billInfo: bill })
        } catch (err) {
            alert("Lỗi")
        }
    }
    render() {
        const { billInfo } = this.state
        return (
            <div>
                <h2>Thông tin hoá đơn</h2>
                {this.state.billInfo ? (
                    <div className="bill-info">
                        <div className="bill-row">
                            <span className="bill-key">Tên phim</span>
                            <span className="bill-value">{billInfo.bill_items[0].ticket.schedule_film.film.name}</span>
                        </div>
                        <div className="bill-row">
                            <span className="bill-key">Số vé</span>
                            <span className="bill-value">{billInfo.bill_items.length}</span>
                        </div>
                        <div className="bill-row">
                            <span className="bill-key">Tổng tiền</span>
                            <span className="bill-value">{billInfo.total_price}</span>
                        </div>
                        <div className="bill-row">
                            <span className="bill-key">Mã ghế</span>
                            <span className="bill-value">{billInfo.bill_items.map(item => { return item.ticket.seat.code}).toString()}</span>
                        </div>
                        <div className="bill-row">
                            <span className="bill-key">Nhân viên bán</span>
                            <span className="bill-value">{billInfo.employee.fullname}</span>
                        </div>
                    </div>
                ) : null}
            </div>

        )
    }
}

export default BillInfo