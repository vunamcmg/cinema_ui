import * as React from "react";

import { connect } from "react-redux";
import { Redirect } from "react-router-dom"
import { General, Sidebar, Films, FilmDetail, ScheduleFilms, Rooms, Statistic, RoomDetail } from './components/index'

import {
    Tooltip,
} from 'react-tippy';

import {
    Header,

    Footer,
} from "../../components/index";
import './manager.css'
import api from '../../services/index'
import { filmAction, scheduleAction, roomAction, seatAction, userAction } from '../../actions'

class Manager extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            showSuccess: false,
            showError: false,
            components: [{
                code: "general", isShow: false
            }, {
                code: "films", isShow: true
            }, {
                code: "film_detail", isShow: false
            }, {
                code: "schedule_film", isShow: false
            }, {
                code: "rooms", isShow: false
            }, {
                code: "statistic", isShow: false
            },
            {
                code: "room_detail", isShow: false
            }
            ],
            selectedFilmId: null,
            selectedRoom: null,
            isLogin: true
        }
        this.openChildComponent = this.openChildComponent.bind(this)
        this.renderChild = this.renderChild.bind(this)
        this.hideSuccess = this.hideSuccess.bind(this)
        this.hideError = this.hideError.bind(this)
        this.deleteFilm = this.deleteFilm.bind(this)
        this.openFilmDetail = this.openFilmDetail.bind(this)
        this.deleteRoom = this.deleteRoom.bind(this)
        this.deleteSeat = this.deleteSeat.bind(this)
        this.deleteScheduleFilm = this.deleteScheduleFilm.bind(this)
        this.openRoomDetail = this.openRoomDetail.bind(this)
        this.checkUserAlreadyLogin = this.checkUserAlreadyLogin.bind(this)
        this.logout = this.logout.bind(this)
    }
    async logout() {
        await localStorage.setItem("isLogin", false)
        await localStorage.setItem("type", null)
        await localStorage.setItem("id", null)
        this.props.dispatch(userAction.logout())
        this.setState({ isLogin: false })
    }
    async deleteScheduleFilm(id) {
        try {
            await api.scheduleFilm.delete(id)
            alert("Xoá lịch chiếu thành công")
            this.props.dispatch(scheduleAction.delete(id))
            this.forceUpdate()
        } catch (err) {
            console.log("err: ", err)
            alert("Xoá lịch chiếu không thành công")
        }
    }
    async deleteRoom(id) {
        try {
            await api.room.delete(id)
            alert("Xoá phòng thành công")
            this.props.dispatch(roomAction.delete(id))
            this.forceUpdate()
        } catch (err) {
            console.log("err: ", err)
            alert("Xoá phòng không thành công")
        }
    }
    async deleteSeat(id) {
        try {
            await api.seat.delete(id)
            alert("Xoá ghế thành công")
            this.props.dispatch(seatAction.delete(id))
            this.forceUpdate()
        } catch (err) {
            console.log("err: ", err)
            alert("Xoá ghế không thành công")
        }
    }
    async openFilmDetail(id) {
        this.setState({ selectedFilmId: id })
        this.openChildComponent("film_detail")
        console.log(this.state);
    }
    async openRoomDetail(item) {
        // this.setState({ selectedFilmId: id })
        this.setState({ selectedRoom: item });
        this.openChildComponent("room_detail")
    }
    async deleteFilm(id) {
        try {
            await api.film.delete(id)
            alert("Xoá phim thành công")
            this.props.dispatch(filmAction.delete(id))
            this.forceUpdate()
        } catch (err) {
            console.log("err: ", err)
            alert("Xoá phim không thành công")
        }

    }
    async hideSuccess() {
        this.setState({ showSuccess: false })
    }
    async hideError() {
        this.setState({ showError: false })
    }
    async componentWillMount() {

    }
    async componentDidUpdate() {

    }
    componentWillReceiveProps(nextProps) {
        return true
    }

    shouldComponentUpdate(nextProps, nextState) {
        return true;
    }
    async componentDidCatch() {

    }
    async componentDidMount() {
        try {
            if (this.props.film.length == 0) {
                let films = await api.film.getList({ query: { fields: ["$all", { "schedule_films": ["$all", { "room": ["$all"] }] }], limit: 1000 } })
                films = films.reverse()
                this.props.dispatch(filmAction.fetch(films))
            }
            if (this.props.scheduleFilm.length == 0) {
                let scheduleFilms = await api.scheduleFilm.getList({ query: { fields: ["$all", { "film": ["$all"] }, { "room": ["$all"] }, { "tickets": ["status", { "seat": ["code"] }] }], limit: 1000 } })

                scheduleFilms = scheduleFilms.reverse()
                this.props.dispatch(scheduleAction.fetch(scheduleFilms))
            }
            if (this.props.room.length == 0) {
                let rooms = await api.room.getList({ query: { fields: ["$all", { "seats": ["$all"] }], limit: 1000 } })
                rooms = rooms.reverse()
                this.props.dispatch(roomAction.fetch(rooms))
            }
        } catch (error) {
            alert("Api error")
        }

    }
    async openChildComponent(code) {
        let newComponent = []
        for (const component of this.state.components) {
            if (component.code === code) {
                component.isShow = true
            } else {
                component.isShow = false
            }
            newComponent.push(component)
        }
        this.setState({ component: newComponent })
    }
    renderChild() {
        for (const component of this.state.components) {
            if (component.isShow) {
                switch (component.code) {
                    case "general":
                        return <General />
                    case "films":
                        return <Films hideError={this.hideError} hideSuccess={this.hideSuccess} {...this.props} deleteFilm={this.deleteFilm} openChildComponent={this.openChildComponent} openFilmDetail={this.openFilmDetail} />
                    case "film_detail":
                        return <FilmDetail {...this.props} editPost={this.editPost} hideError={this.hideError} hideSuccess={this.hideSuccess} selectedFilmId={this.state.selectedFilmId} />
                    case "schedule_film":
                        return <ScheduleFilms {...this.props} hideError={this.hideError} hideSuccess={this.hideSuccess} deleteScheduleFilm={this.deleteScheduleFilm} />
                    case "rooms":
                        return <Rooms {...this.props} hideError={this.hideError} hideSuccess={this.hideSuccess} deleteRoom={this.deleteRoom} openRoomDetail={this.openRoomDetail} openChildComponent={this.openChildComponent} />
                    case "statistic":
                        return <Statistic hideError={this.hideError} hideSuccess={this.hideSuccess} />
                    case "room_detail":
                        return <RoomDetail {...this.props} hideError={this.hideError} hideSuccess={this.hideSuccess} deleteSeat={this.deleteSeat} openRoomDetail={this.openRoomDetail} selectedRoom={this.state.selectedRoom} />
                }
            }
        }

    }
    async checkUserAlreadyLogin() {
        const isLogin = localStorage.getItem('isLogin')
        if (isLogin !== "true") {
            this.setState({ isLogin: false })
        }
    }
    render() {
        this.checkUserAlreadyLogin()
        if (!this.state.isLogin) {
            return <Redirect to="/login" />
        }
        const ChildComponent = this.renderChild()
        console.log("props: ", this.props)
        return (
            <div>
                <div className="admin-navbar">
                    <h1 className="admin-nav-item admin-nav-brand">Lotte Cinema Manager</h1>
                    <div className="admin-navbar-right">
                        <Tooltip
                            title="Thông báo"
                            position="top"
                        >
                            <button className="admin-navbar-button"><i class="far fa-bell"></i></button>
                        </Tooltip>
                        <Tooltip
                            title="Thư"
                            position="top"
                        >
                            <button className="admin-navbar-button"><i class="far fa-envelope"></i></button>
                        </Tooltip>
                        <Tooltip
                            title="Bình luận"
                            position="top"
                        >
                            <button className="admin-navbar-button"><i class="far fa-comment"></i></button>
                        </Tooltip>
                        <Tooltip
                            title="Đăng xuất"
                            position="top"
                        >
                            <button onClick={this.logout} className="admin-navbar-button"><i class="fas fa-sign-out-alt"></i></button>
                        </Tooltip>
                    </div>
                    <div>

                    </div>
                </div>
                <div className="admin-main">
                    <Sidebar>
                        <ul>
                            {/* <li className="admin-sidebar-link" onClick={() => this.openChildComponent("general")}> Tổng quan </li> */}
                            <li className="admin-sidebar-link" onClick={() => { return this.props.history.push("/booking") }}> Trang đặt phòng</li>
                            <li className="admin-sidebar-link" onClick={() => this.openChildComponent("films")}> Phim </li>
                            <li className="admin-sidebar-link" onClick={() => this.openChildComponent("rooms")}> Phòng chiếu </li>
                            <li className="admin-sidebar-link" onClick={() => this.openChildComponent("schedule_film")}> Lịch chiếu </li>
                            <li className="admin-sidebar-link" onClick={() => this.openChildComponent("statistic")}> Thống kê </li>
                        </ul>
                    </Sidebar>
                    <div className="admin-block">
                        {ChildComponent}
                    </div>
                </div>
            </div>
        );
    }

}

const mapStateToProps = (state) => {
    return state;
};

export default connect(mapStateToProps)(Manager);
