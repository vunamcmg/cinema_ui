import * as React from "react";

import "./edit.css";
import api from '../../../../../../services'
import imgurService from '../../../../../../services/imgur'
import CKEditor from "react-ckeditor-component";
import { roomAction, seatAction } from '../../../../../../actions'
import { BaseModal } from '../../../../../../modals'
import { TextInput, SelectInput } from '../../../../../../elements'
class Edit extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: "",
            "sub_fee": 0
        }
        this.update = this.update.bind(this)
        this.onChange = this.onChange.bind(this)
        this.handleFileInputChange = this.handleFileInputChange.bind(this)
        this.onFormChange = this.onFormChange.bind(this)
        this.handleInputChange = this.handleInputChange.bind(this)
    }
    async handleInputChange(field, value) {
        this.setState({ [field]: value })
    }
    async handleFileInputChange(event) {
        this.setState({ selectedFile: event.target.files[0] })
    }
    onFormChange(key) {
        this.setState({ [key]: this.refs[key].value })
    }
    onChange(evt) {
        var newContent = evt.editor.getData();
        this.setState({
            description: newContent
        })
    }
    async update(event) {
        event.preventDefault();
        try {

            const body = {
                sub_fee: this.state.sub_fee
            }
            const result = await api.seat.update(this.state.id, body)
            this.props.dispatch(seatAction.update(result))
            alert("Cập nhật ghế thành công")

            var x = setTimeout(this.props.handleCloseEdit(), 1000)
            clearTimeout(x)
        } catch (error) {
            console.log('err: ', error)
            alert("Cập nhật không thành công")
        }
    }
    async componentDidMount() {
        const seat = this.props.seat.find((item) => { return item.id == this.props.id })
        this.setState(seat)
    }
    componentWillReceiveProps(nextProps) {
        if (this.state.id !== nextProps.id) {
            return true
        }
        return false
    }

    shouldComponentUpdate(nextProps, nextState) {
        return true;
    }
    render() {
        console.log("state: ", this.state)
        return (
            <BaseModal show={true} handleClose={this.props.handleCloseEdit} style={{ height: "415px", width: "700px" }}>
                <div className="edit-main">
                    <form onSubmit={this.update} id="add-post-form" encType="multipart/form-data">
                       
                        <TextInput label="Phụ phí" refKey="sub_fee" placeholder="0" value={this.state.number} style={{}} handleInputChange={this.handleInputChange} required={true} />
                        <button type="submit" className="btn btn-primary" style={{ width: "150px", "margin-top": "14px" }}>
                            Cập nhật
                        </button>

                    </form>
                </div>
            </BaseModal>
        );
    }
}

export default Edit;
