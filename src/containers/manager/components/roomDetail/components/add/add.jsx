import * as React from "react";

import "./add.css";
import api from '../../../../../../services'
import imgurService from '../../../../../../services/imgur'
import CKEditor from "react-ckeditor-component";
import { roomAction, seatAction } from '../../../../../../actions'
import { BaseModal } from '../../../../../../modals'
import { TextInput, SelectInput } from '../../../../../../elements'
class Add extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: "",
            "room_id": '',
            "code": "",
            "sub_fee": 0
        }
        this.add = this.add.bind(this)
        this.onChange = this.onChange.bind(this)
        this.handleFileInputChange = this.handleFileInputChange.bind(this)
        this.onFormChange = this.onFormChange.bind(this)
        this.handleInputChange = this.handleInputChange.bind(this)
    }
    async handleInputChange(field, value) {
        this.setState({ [field]: value })
    }
    async handleFileInputChange(event) {
        this.setState({ selectedFile: event.target.files[0] })
    }
    onFormChange(key) {
        this.setState({ [key]: this.refs[key].value })
    }
    onChange(evt) {
        var newContent = evt.editor.getData();
        this.setState({
            description: newContent
        })
    }
    async add(event) {
        event.preventDefault();
        try {

            const body = {
                room_id: this.props.room_id,
                code: this.state.code,
                sub_fee: this.state.sub_fee
            }
            const result = await api.seat.add(body)
            console.log("@@@@@@@@@@@@@@@@@@@@@@@@");
            console.log(result);
            this.props.dispatch(seatAction.unshift(result));
            alert("Thêm ghế thành công")

            var x = setTimeout(this.props.handleCloseAdd(), 1000)
            clearTimeout(x)
        } catch (error) {
            console.log('err: ', error)
            alert("Thêm không thành công")
        }
    }
    async componentDidMount() {
        // const room = this.props.room.find((item) => { return item.id == this.props.id })

        // this.setState(room)
    }
    componentWillReceiveProps(nextProps) {
        if (this.state.id !== nextProps.id) {
            return true
        }
        return false
    }

    shouldComponentUpdate(nextProps, nextState) {
        return true;
    }
    render() {

        return (
            <BaseModal show={true} handleClose={this.props.handleCloseAdd} style={{ height: "415px", width: "700px" }}>
                <div className="edit-main">
                    <form onSubmit={this.add} id="add-post-form" encType="multipart/form-data">

                        <TextInput label="Mã ghế" refKey="code" placeholder="A1" value={this.state.code} style={{}} handleInputChange={this.handleInputChange} required={true} />
                        <TextInput label="Phụ phí" refKey="sub_fee" placeholder="0" value={this.state.sub_fee} style={{}} handleInputChange={this.handleInputChange} required={true} />

                        <button type="submit" className="btn btn-primary" style={{ width: "150px", "margin-top": "14px" }}>
                            Cập nhật
                        </button>

                    </form>
                </div>
            </BaseModal>
        );
    }
}

export default Add;
