import * as React from "react";
import { Route, Link, Router } from "react-router-dom";
import api from '../../../../services/index'

import { Edit, Add } from './components'
import * as _ from 'lodash'
import "./roomDetail.css";

import {
    Tooltip,
} from 'react-tippy';

import { roomAction } from '../../../../actions'
class RoomDetail extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            posts: [],
            isShowAddPost: false,
            content: 'Nội dung của bạn',
            selectedFile: null,
            showEdit: false,
            showAdd: false,
            seats: [],
            selectedRoomId: null,
            selectedSeatId: null
        };
        this.edit = this.edit.bind(this)
        this.open = this.open.bind(this)
        this.delete = this.delete.bind(this)
        this.add = this.add.bind(this)
        this.handleCloseAdd = this.handleCloseAdd.bind(this)
        this.handleCloseEdit = this.handleCloseEdit.bind(this)
    }
    async add() {
        this.setState({ showAdd: true })
    }
    async delete(id) {
        this.props.deleteSeat(id)
    }
    async open(id) {
        //this.setState({ selectedFilmId: id })
        this.props.openFilmDetail(id)
        // window.open(trailer, "_blank")
    }
    async edit(id) {
        console.log("id phim: ", id)
        this.setState({ showEdit: true, selectedSeatId: id })
    }
    handleCloseAdd() {
        this.setState({ showAdd: false })
    }
    handleCloseEdit() {
        this.setState({ showEdit: false })
    }


    componentWillReceiveProps(nextProps) {
        return true
    }

    shouldComponentUpdate(nextProps, nextState) {
        return true;
    }
    componentDidMount() {

    }

    render() {
        return (
            <div className="wrap">
                <b>Danh sách ghế</b>
                <div className="film-toolbar">
                {this.state.showEdit ? <Edit id={this.state.selectedSeatId} handleCloseEdit={this.handleCloseEdit} {...this.props} /> : null}
                {this.state.showAdd ? <Add handleCloseAdd={this.handleCloseAdd} {...this.props} room_id = {this.props.selectedRoom.id} /> : null}
                    <button className="film-toolbar-button" onClick={this.add}>Thêm ghế</button>
                    {/* <button className="film-toolbar-button" onClick={this.addFilm}>Thống kê</button> */}
                </div>
                <table className="admin-posts-table">
                    <tbody>
                        <tr>
                            <th>Thứ tự</th>
                            <th>Mã</th>
                            <th>Phụ phí</th>
                            <th>Thao tác</th>
                        </tr>
                        {this.props.seat.map((item, index) => {
                            return (
                                <tr key={item.id} >
                                    <td>{index + 1}</td>
                                    <td>{item.code}</td>
                                    <td>{item.sub_fee}</td>
                                    <td className="action-td">
                                        <Tooltip
                                            title="Chỉnh sửa"
                                            position="left"
                                        >
                                            <span className="post-edit-button" onClick={() => this.edit(item.id)}> <i class="fas fa-pen"></i> </span>
                                        </Tooltip>
                                        <Tooltip
                                            title="Xoá"
                                            position="left"
                                        >
                                            <span className="post-remove-button" onClick={() => this.delete(item.id)} ><i class="fas fa-times"></i></span>
                                        </Tooltip>
                                    </td>
                                </tr>

                            )
                        })}
                    </tbody>
                </table>
            </div>
        );

    }
}


export default RoomDetail
