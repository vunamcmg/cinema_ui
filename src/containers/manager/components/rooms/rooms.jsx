import * as React from "react";
import { Route, Link, Router } from "react-router-dom";
import api from '../../../../services/index'

import { Edit, Add } from './components'
import * as _ from 'lodash'
import "./rooms.css";

import {
    Tooltip,
} from 'react-tippy';

import { roomAction, seatAction } from '../../../../actions'
class Rooms extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            posts: [],
            isShowAddPost: false,
            content: 'Nội dung của bạn',
            selectedFile: null,
            showEdit: false,
            showAdd: false,
            selectedRoomId: null
        };
        this.edit = this.edit.bind(this)
        this.open = this.open.bind(this)
        this.delete = this.delete.bind(this)
        this.add = this.add.bind(this)
        this.handleCloseAdd = this.handleCloseAdd.bind(this)
        this.handleCloseEdit = this.handleCloseEdit.bind(this)
    }
    async add() {
        this.setState({ showAdd: true })
    }
    async delete(id) {
        this.props.deleteRoom(id)
    }
    async open(item) {
        console.log(item);
        this.props.dispatch(seatAction.fetch(item.seats))
        this.props.openRoomDetail(item);
    }
    async edit(id) {
        console.log("id phim: ", id)
        this.setState({ showEdit: true, selectedRoomId: id })
    }
    handleCloseAdd() {
        this.setState({ showAdd: false })
    }
    handleCloseEdit() {
        this.setState({ showEdit: false })
    }


    componentWillReceiveProps(nextProps) {
        return true
    }

    shouldComponentUpdate(nextProps, nextState) {
        return true;
    }
    componentDidMount() {

    }

    // async open(id) {
    //     //this.setState({ selectedFilmId: id })
    //     this.props.openChildComponent("room_detail");
    //     // window.open(trailer, "_blank")
    // }

    render() {
        console.log("props: ", this.props)
        const { room } = this.props
        return (

            <div className="add-post-main">
                {this.state.showEdit ? <Edit id={this.state.selectedRoomId} handleCloseEdit={this.handleCloseEdit} {...this.props} /> : null}
                {this.state.showAdd ? <Add handleCloseAdd={this.handleCloseAdd} {...this.props} /> : null}
                <div className="film-toolbar">
                    <button className="film-toolbar-button" onClick={this.add}>Thêm phòng</button>
                    {/* <button className="film-toolbar-button" onClick={this.addFilm}>Thống kê</button> */}
                </div>

                {this.state.isShowAddPost ? <div>
                    <form onSubmit={this.addPost} id="add-post-form" encType="multipart/form-data">
                        <div className="form-group">
                            <input
                                type="text"
                                className="form-control contact-input"
                                placeholder="Tiêu đề"
                                ref="title"
                                required
                            />
                        </div>
                        <div className="form-group">
                            <input
                                type="text"
                                className="form-control contact-input"
                                placeholder="Tác giả"
                                ref="author"
                                required
                            />
                        </div>
                        <div className="form-group">
                            <input
                                type="file"
                                className="form-control contact-input"
                                placeholder="Thumbnail"
                                ref="thumb"
                                required
                                onChange={this.handleFileInputChange}
                            />
                        </div>
                        {/* <CKEditor
                            activeClass="p10"
                            content={this.state.content}
                            events={{
                                "blur": this.onBlur,
                                "afterPaste": this.afterPaste,
                                "change": this.onChange
                            }}
                        /> */}

                        <button type="submit" className="btn btn-primary">
                            Đăng bài
                            </button>

                    </form>
                </div> : null}
                <div className="posts">
                    <table className="admin-posts-table">
                        <tbody>
                            <tr>
                                <th>Thứ tự</th>
                                <th>Tên</th>
                                <th>Loại phòng</th>
                                <th>Số ghế</th>
                                <th>Thao tác</th>
                            </tr>
                            {room.map((item, index) => {
                                return (
                                    <tr key={item.id} >
                                        <td>{index + 1}</td>
                                        <td >{item.name}</td>
                                        <td>{item.type == 'FOUR_WAY_SOUND'? 'Âm thanh 4 chiều': (item.type =='BIG_SCREEN' ? 'Màn hình lớn': item.type)}</td>
                                        <td>{item.seats.length}</td>
                                        <td className="action-td">
                                            <Tooltip
                                                title="Chi tiết"
                                                position="top"
                                            >
                                                <span className="post-open-button" onClick={() => this.open(item)}><i class="fas fa-share-square"></i></span>
                                            </Tooltip>
                                            <Tooltip
                                                title="Chỉnh sửa"
                                                position="top"
                                            >
                                                <span className="post-edit-button" onClick={() => this.edit(item.id)}> <i class="fas fa-pen"></i> </span>
                                            </Tooltip>
                                            <Tooltip
                                                title="Xoá"
                                                position="top"
                                            >
                                                <span className="post-remove-button" onClick={() => this.delete(item.id)}><i class="fas fa-times"></i></span>
                                            </Tooltip>
                                        </td>
                                    </tr>

                                )
                            })}
                        </tbody>
                    </table>
                </div>
            </div>
        );

    }
}


export default Rooms
