import * as React from "react";

import "./add.css";
import api from '../../../../../../services'
import imgurService from '../../../../../../services/imgur'
import CKEditor from "react-ckeditor-component";
import { roomAction } from '../../../../../../actions'
import { BaseModal } from '../../../../../../modals'
import { TextInput, SelectInput } from '../../../../../../elements'
class Add extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: "",
            "name": null,
            "type": "2D",
            "number": null
        }
        this.add = this.add.bind(this)
        this.onChange = this.onChange.bind(this)
        this.handleFileInputChange = this.handleFileInputChange.bind(this)
        this.onFormChange = this.onFormChange.bind(this)
        this.handleInputChange = this.handleInputChange.bind(this)
    }
    async handleInputChange(field, value) {
        this.setState({ [field]: value })
    }
    async handleFileInputChange(event) {
        this.setState({ selectedFile: event.target.files[0] })
    }
    onFormChange(key) {
        this.setState({ [key]: this.refs[key].value })
    }
    onChange(evt) {
        var newContent = evt.editor.getData();
        this.setState({
            description: newContent
        })
    }
    async add(event) {
        event.preventDefault();
        try {

            const body = {
                name: this.state.name,
                type: this.state.type,
                number: this.state.number
            }
            const result = await api.room.add(body)
            let data = result.room;
            data.seats = result.seats;
            this.props.dispatch(roomAction.unshift(data));
            alert("Thêm phòng thành công")
            var x = setTimeout(this.props.handleCloseAdd(), 1000)
            clearTimeout(x)
        } catch (error) {
            console.log('err: ', error)
            alert("Thêm không thành công")
        }
    }
    async componentDidMount() {
        // const room = this.props.room.find((item) => { return item.id == this.props.id })
       
        // this.setState(room)
    }
    componentWillReceiveProps(nextProps) {
        if (this.state.id !== nextProps.id) {
            return true
        }
        return false
    }

    shouldComponentUpdate(nextProps, nextState) {
        return true;
    }
    render() {
        const roomType = [
            {
                name: "2D",
                id: "2D"
            }, {
                name: "3D",
                id: "3D"
            },
            {
                name: "Màn hình lớn",
                id: "BIG_SCREEN"
            },
            {
                name: "Âm thanh 4 chiều",
                id: "FOUR_WAY_SOUND"
            }
        ]
        return (
            <BaseModal show={true} handleClose={this.props.handleCloseAdd} style={{ height: "415px", width: "700px" }}>
                <div className="edit-main">
                    <form onSubmit={this.add} id="add-post-form" encType="multipart/form-data">
                       
                        <TextInput label="Tên phòng chiếu" refKey="name" placeholder="LOTTE" value={this.state.name} style={{}} handleInputChange={this.handleInputChange} required={true}/>
                        {/* <TextInput label="Số lượng ghế" refKey="number" placeholder="Số lượng ghế" value={this.state.number} style={{}} handleInputChange={this.handleInputChange} required={true} /> */}
                        <SelectInput label="Loại phòng" refKey="type" data={roomType} titleKey="name" valueKey="id" handleInputChange={this.handleInputChange} />

                        <button type="submit" className="btn btn-primary" style={{ width: "150px", "margin-top": "14px" }}>
                            Cập nhật
                        </button>

                    </form>
                </div>
            </BaseModal>
        );
    }
}

export default Add;
