import * as React from "react";

import "./filmDetail.css";
import api from '../../../../services'
import imgurService from '../../../../services/imgur'
import CKEditor from "react-ckeditor-component";
import { filmAction, scheduleAction } from '../../../../actions'
import { AddScheduleFilm, ScheduleFilms } from './components'
import { TextInput, SelectInput } from '../../../../elements'

import {
    Tooltip,
} from 'react-tippy';
class FilmDetail extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: "",
            "name": null,
            "start_time": null,
            "description": null,
            "avatar": null,
            "trailer": null,
            status: null,
            content: "Nội dung",
            selectedFile: null,
            schedule_films: [],
            showScheduleFilm: false
        }
        this.update = this.update.bind(this)
        this.onChange = this.onChange.bind(this)
        this.handleFileInputChange = this.handleFileInputChange.bind(this)
        this.onFormChange = this.onFormChange.bind(this)
        this.showAddScheduleFilm = this.showAddScheduleFilm.bind(this)
        this.handleCloseAddScheduleFilm = this.handleCloseAddScheduleFilm.bind(this)
        this.addScheduleFilm = this.addScheduleFilm.bind(this)
        this.deleteScheduleFilm = this.deleteScheduleFilm.bind(this)
        this.handleInputChange = this.handleInputChange.bind(this)
    }
    async handleInputChange(field, value) {
        this.setState({ [field]: value })
    }
    addScheduleFilm(data) {
        this.setState({ schedule_films: this.state.schedule_films.concat(data) })
    }
    async deleteScheduleFilm(id) {
        try {
            await api.scheduleFilm.delete(id)
            alert("Xoá lịch chiếu thành công")
            this.props.dispatch(scheduleAction.delete(id))
            const scheduleIndex = this.state.schedule_films.findIndex(item => { return item.id === id })
            this.state.schedule_films.splice(scheduleIndex, 1)
            this.setState({ schedule_films: this.state.schedule_films })
            this.forceUpdate()
        } catch (err) {
            console.log("err: ", err)
            alert("Xoá lịch chiếu không thành công")
        }
    }
    showAddScheduleFilm() {
        this.setState({ showScheduleFilm: true })
    }
    async handleCloseAddScheduleFilm() {
        this.setState({ showScheduleFilm: false })
    }
    async handleFileInputChange(event) {
        this.setState({ selectedFile: event.target.files[0] })
    }
    onFormChange(key) {
        this.setState({ [key]: this.refs[key].value })
    }
    onChange(evt) {
        var newContent = evt.editor.getData();
        this.setState({
            description: newContent
        })
    }
    async update(event) {
        event.preventDefault();
        try {
            let link = this.state.avatar
            if (this.state.selectedFile !== null) {
                link = await imgurService.uploadImage(this.state.selectedFile)
            }
            const body = {
                name: this.refs.name.value,
                description: this.state.description,
                start_time: this.refs.start_time.value,
                trailer: this.refs.trailer.value,
                avatar: this.refs.avatar.value,
                status: this.state.status
            }
            const result = await api.film.update(this.state.id, body)
            this.props.dispatch(filmAction.update({
                id: this.state.id,
                name: this.refs.name.value,
                avatar: this.refs.avatar.value,
                description: this.state.description,
                start_time: this.refs.start_time.value,
                trailer: this.refs.trailer.value,
            }))
            alert("Cập nhật bài viết thành công")
        } catch (error) {
            console.log('err: ', error)
            alert("Cập nhật không thành công")
        }
    }
    async componentDidMount() {
        window.scrollTo(0, 0);
        const film = this.props.film.find((item) => { return item.id == this.props.selectedFilmId })
        this.setState(film)

    }
    componentWillReceiveProps(nextProps) {
        if (this.state.id !== nextProps.id) {
            return true
        }
        return false
    }

    shouldComponentUpdate(nextProps, nextState) {
        return true;
    }
    render() {
        const filmStatus = [
            {
                id: "WILL_BE_RELEASE",
                name: "Sắp chiếu"
            },
            {
                id: "RELEASING",
                name: "Đang chiếu"
            },
            {
                id: "RELEASED",
                name: "Dừng công chiếu"
            }
        ]
        return (
            <div className="add-post-main">
                <div className="wrapper-film">
                    {this.state.showScheduleFilm ? <AddScheduleFilm handleCloseAdd={this.handleCloseAddScheduleFilm} addScheduleFilm={this.addScheduleFilm} {...this.props} {...this.state} /> : null}
                    <form onSubmit={this.update} id="add-post-form" encType="multipart/form-data">
                        <div className="form-group">
                            <input
                                type="text"
                                className="form-control contact-input"
                                placeholder="Tên phim"
                                ref="name"
                                defaultValue={this.state.name}
                                required
                            />
                        </div>
                        <div className="form-group">
                            <input
                                type="text"
                                className="form-control contact-input"
                                placeholder="Ngày khởi chiếu"
                                ref="start_time"
                                defaultValue={this.state.start_time}
                                required
                            />
                        </div>
                        <div className="form-group">
                            <input
                                type="text"
                                className="form-control contact-input"
                                placeholder="Trailer"
                                ref="trailer"
                                defaultValue={this.state.trailer}
                                required
                            />
                        </div>
                        <div className="form-group">
                            <input
                                type="text"
                                className="form-control contact-input"
                                placeholder="Avatar"
                                ref="avatar"
                                defaultValue={this.state.avatar}
                                required
                            />
                        </div>
                        <SelectInput label="Tình trạng" refKey="status" data={filmStatus} titleKey="name" valueKey="id" handleInputChange={this.handleInputChange} />
                        <CKEditor
                            activeClass="p10"
                            content={this.state.description}
                            events={{
                                "blur": this.onBlur,
                                "afterPaste": this.afterPaste,
                                "change": this.onChange
                            }}
                            className="edit-content"
                        />

                        <button type="submit" className="btn btn-primary" style={{ width: "150px", "margin-top": "14px" }}>
                            Cập nhật
                        </button>

                    </form>
                    <div className="section-header">
                        <h4>Danh sách lịch chiếu</h4>
                        <Tooltip
                            title="Thêm lịch chiếu"
                            position="top"
                        >
                            <i className="fa fa-plus" onClick={this.showAddScheduleFilm}></i>
                        </Tooltip>

                    </div>
                    <hr />
                    <div>
                        <ScheduleFilms {...this.state} {...this.props} showAddScheduleFilm={this.showScheduleFilm} deleteScheduleFilm={this.deleteScheduleFilm} />
                    </div>
                </div>
            </div>
        );
    }
}

export default FilmDetail;
