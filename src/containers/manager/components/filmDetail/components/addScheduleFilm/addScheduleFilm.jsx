import * as React from "react";
import * as moment from 'moment'

import "./addScheduleFilm.css";
import api from '../../../../../../services'
import imgurService from '../../../../../../services/imgur'
import CKEditor from "react-ckeditor-component";
import { scheduleAction } from '../../../../../../actions'
import { BaseModal } from '../../../../../../modals'
import { TextInput, SelectInput } from '../../../../../../elements/index'
import DatePicker from "react-datepicker";
class AddScheduleFilm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            filmId: null,
            roomId: null,
            price: 50000,
            memberPrice: 40000,
            start_time: moment().format(''),
            rooms: []
        }
        this.add = this.add.bind(this)
        this.onChange = this.onChange.bind(this)
        this.handleFileInputChange = this.handleFileInputChange.bind(this)
        this.onFormChange = this.onFormChange.bind(this)
        this.handleInputChange = this.handleInputChange.bind(this)
        this.handleChange = this.handleChange.bind(this)
    }
    handleChange(date) {
        this.setState({
            start_time: moment(date).format()
        });
    }
    async handleInputChange(field, value) {
        this.setState({ [field]: value })
    }
    async handleFileInputChange(event) {
        this.setState({ selectedFile: event.target.files[0] })
    }
    onFormChange(key) {
        this.setState({ [key]: this.refs[key].value })
    }
    onChange(evt) {
        var newContent = evt.editor.getData();
        this.setState({
            description: newContent
        })
    }
    async add(event) {
        event.preventDefault();
        try {
            const body = {
                film_id: this.state.filmId,
                room_id: this.state.roomId,
                price: this.state.price,
                start_time:this.state.start_time,
                price_member: this.state.memberPrice
            }
            let room = this.state.rooms.find((item) => { return item.id === this.state.roomId })
            const result = await api.ticket.createScheduleFilm(body)
            result.room = {}
            result.room.name = room.name
            result.room.type = room.type
            alert("Thêm lịch chiếu thành công")
            this.props.addScheduleFilm(result)
            this.props.handleCloseAdd()
        } catch (error) {
            console.log('err: ', error)
            alert("Thêm không thành công")
        }
    }
    async componentDidMount() {
        const rooms = this.props.room
        this.setState({ rooms: rooms, roomId: rooms[0].id, filmId: this.props.id })
    }
    componentWillReceiveProps(nextProps) {
        if (this.state.id !== nextProps.id) {
            return true
        }
        return false
    }

    shouldComponentUpdate(nextProps, nextState) {
        return true;
    }
    render() {
        return (

            <BaseModal show={true} handleClose={this.props.handleCloseAdd} style={{ height: "500px", width: "700px" }}>
                <div className="edit-main">
                    <form onSubmit={this.add} id="add-post-form" encType="multipart/form-data">
                   
                        <div className="form-group date-picker" >
                            <label>Ngày khởi chiếu</label>
                            {/* <br/> */}
                                <DatePicker
                                    className="contact-input"
                                    selected={this.state.start_time}
                                    onChange={this.handleChange}
                                    showTimeSelect
                                    timeFormat="HH:mm"
                                    timeIntervals={15}
                                    dateFormat="MMMM d, yyyy h:mm aa"
                                    timeCaption="time"
                                />
                        </div>
                       
                        <br/>
                        <TextInput label="Giá vé" refKey="price" placeholder="Giá vé thường" value="50000" style={{}} handleInputChange={this.handleInputChange} />
                        <TextInput label="Giá vé cho thành viên" refKey="memberPrice" placeholder="Giá vé cho thành viên" value="40000" style={{}} handleInputChange={this.handleInputChange} />
                        <SelectInput label="Phòng chiếu" refKey="roomId" data={this.props.room} titleKey="name" valueKey="id" handleInputChange={this.handleInputChange} />
                        <button type="submit" className="btn btn-primary" style={{ width: "150px", "margin-top": "14px" }}>
                            Thêm lịch chiếu
                        </button>

                    </form>
                </div>
            </BaseModal>
        );
    }
}

export default AddScheduleFilm;
