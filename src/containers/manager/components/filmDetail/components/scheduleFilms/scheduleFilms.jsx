import * as React from "react";
import {
    Tooltip,
} from 'react-tippy';
import * as moment from 'moment'

import api from '../../../../../../services'
import { scheduleAction } from '../../../../../../actions'
import "./scheduleFilms.css";
class ScheduleFilms extends React.Component {
    constructor(props) {
        super(props)
        this.open = this.open.bind(this)
        this.delete = this.delete.bind(this)
    }
    async delete(id) {
        try {
            await api.scheduleFilm.delete(id)
            alert("Xoá lịch chiếu thành công")
            this.props.dispatch(scheduleAction.delete(id))

            this.forceUpdate()
        } catch (err) {
            console.log("err: ", err)
            alert("Xoá lịch chiếu không thành công")
        }
    }
    async open(trailer) {
        window.open(trailer, "_blank")
    }
    async componentDidMount() {

    }

    componentWillReceiveProps(nextProps) { }

    shouldComponentUpdate(nextProps, nextState) {
        return true;
    }
    render() {
        const { schedule_films } = this.props
        return (
            <div className="posts">
                <table className="admin-posts-table">
                    <tbody>
                        <tr>
                            <th>Thứ tự</th>
                            <th>Phòng</th>
                            <th>Ngày chiếu</th>
                            <th>Loại phòng</th>
                            <th>Thao tác</th>
                        </tr>
                        {schedule_films.map((item, index) => {
                            return (
                                <tr key={item.id} >
                                    <td>{index + 1}</td>
                                    <td >{item.room ? item.room.name : ""}</td>
                                    <td>{moment(item.start_time).utc().format('DD/MM/YYYY HH:mm')}</td>
                                    <td>{item.room ? (item.room.type == 'FOUR_WAY_SOUND' ? 'Âm thanh 4 chiều' : (item.room.type == 'BIG_SCREEN' ? 'Màn hình lớn' : item.room.type)) : ''}</td>
                                    <td className="action-td">
                                        <Tooltip
                                            title="Xem trailer"
                                            position="top"
                                        >
                                            <span className="post-open-button" onClick={() => this.open(item.trailer)}><i class="fas fa-share-square"></i></span>
                                        </Tooltip>
                                        <Tooltip
                                            title="Chỉnh sửa"
                                            position="top"
                                        >
                                            <span className="post-edit-button" onClick={() => this.props.selectFilm(item.id)}> <i class="fas fa-pen"></i> </span>
                                        </Tooltip>
                                        <Tooltip
                                            title="Xoá"
                                            position="top"
                                        >
                                            <span className="post-remove-button" onClick={() => this.props.deleteScheduleFilm(item.id)}><i class="fas fa-times"></i></span>
                                        </Tooltip>
                                    </td>
                                </tr>

                            )
                        })}
                    </tbody>
                </table>
            </div>
        )
    }
}

export default ScheduleFilms