import * as React from "react";
import "./sidebar.css";
class Sidebar extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {

    }
    render() {

        return (
            <div className="sidebar">
                {this.props.children}
            </div>
        );
    }
}

export default Sidebar;
