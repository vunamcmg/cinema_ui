import * as React from "react";
import { Route, Link, Router } from "react-router-dom";
import api from '../../../../services/index'

import { Edit } from './components'
import * as _ from 'lodash'
import * as moment from 'moment'
import "./scheduleFilms.css";

import {
    Tooltip,
} from 'react-tippy';

import { scheduleAction } from '../../../../actions'
class ScheduleFilm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            posts: [],
            isShowAddPost: false,
            content: 'Nội dung của bạn',
            selectedFile: null,
            showEdit: false,
            showAdd: false,
            selectedScheduleId: null
        };
        this.edit = this.edit.bind(this)
        this.open = this.open.bind(this)
        this.edit = this.edit.bind(this)
        this.delete = this.delete.bind(this)
        this.handleCloseAdd = this.handleCloseAdd.bind(this)
        this.handleCloseEdit = this.handleCloseEdit.bind(this)
    }
    async edit(id) {
        this.setState({ selectedScheduleId: id })
        this.setState({ showEdit: true })
    }
    async delete(id) {
        this.props.deleteScheduleFilm(id)
    }
    async open(trailer) {
        //this.setState({ selectedFilmId: id })

        window.open(trailer, "_blank")
    }
    async edit(id) {
        this.setState({ showEdit: true, selectedScheduleId: id })
    }

    handleCloseAdd() {
        this.setState({ showAdd: false })
    }
    handleCloseEdit() {
        this.setState({ showEdit: false })
    }


    componentWillReceiveProps(nextProps) {
        return true
    }

    shouldComponentUpdate(nextProps, nextState) {
        return true;
    }
    async componentDidMount() {


    }

    render() {
        const { scheduleFilm } = this.props
        return (

            <div className="add-post-main">

                {this.state.showEdit ? <Edit id={this.state.selectedScheduleId} handleCloseEdit={this.handleCloseEdit} {...this.props} /> : null}
                {/* {this.state.showAdd ? < handleCloseAdd={this.handleCloseAdd} {...this.props} /> : null} */}

                {this.state.isShowAddPost ? <div>
                    <form onSubmit={this.addPost} id="add-post-form" encType="multipart/form-data">
                        <div className="form-group">
                            <input
                                type="text"
                                className="form-control contact-input"
                                placeholder="Tiêu đề"
                                ref="title"
                                required
                            />
                        </div>
                        <div className="form-group">
                            <input
                                type="text"
                                className="form-control contact-input"
                                placeholder="Tác giả"
                                ref="author"
                                required
                            />
                        </div>
                        <div className="form-group">
                            <input
                                type="file"
                                className="form-control contact-input"
                                placeholder="Thumbnail"
                                ref="thumb"
                                required
                                onChange={this.handleFileInputChange}
                            />
                        </div>
                        {/* <CKEditor
                            activeClass="p10"
                            content={this.state.content}
                            events={{
                                "blur": this.onBlur,
                                "afterPaste": this.afterPaste,
                                "change": this.onChange
                            }}
                        /> */}

                        <button type="submit" className="btn btn-primary">
                            Đăng bài
                            </button>

                    </form>
                </div> : null}
                <div className="posts">
                    <table className="admin-posts-table">
                        <tbody>
                            <tr>
                                <th>Thứ tự</th>
                                <th>Phim</th>
                                <th>Phòng</th>
                                <th>Ngày chiếu</th>
                                <th>Loại phòng</th>
                                <th>Vé</th>
                                <th>Giá thường</th>
                                <th>Giá thành viên</th>
                                <th>Thao tác</th>
                            </tr>
                            {scheduleFilm.map((item, index) => {
                                const soldTicket = item.tickets.filter(item => { return item.status === "SOLD" }).length
                                const ticketAmount = item.tickets.length
                                return (
                                    <tr key={item.id} >
                                        <td>{index + 1}</td>
                                        <td>{item.film_id && item.film ? item.film.name : null}</td>
                                        <td >{item.room ? item.room.name : null}</td>
                                        <td>{moment(item.start_time).utc().format('DD/MM/YYYY HH:mm')}</td>
                                        <td>{item.room ? (item.room.type == 'FOUR_WAY_SOUND' ? 'Âm thanh 4 chiều' : (item.room.type == 'BIG_SCREEN' ? 'Màn hình lớn' : item.room.type)) : ''}</td>
                                        <td>{soldTicket}/{ticketAmount}</td>
                                        <td>{item.price}</td>
                                        <td>{item.price_member}</td>
                                        <td className="action-td">
                                            <Tooltip
                                                title="Xem trailer"
                                                position="top"
                                            >
                                                <span className="post-open-button" onClick={() => this.open(item.film_id ? item.film.trailer : null)}><i class="fas fa-share-square"></i></span>
                                            </Tooltip>
                                            <Tooltip
                                                title="Chỉnh sửa"
                                                position="top"
                                            >
                                                <span className="post-edit-button" onClick={() => this.edit(item.id)}> <i class="fas fa-pen"></i> </span>
                                            </Tooltip>
                                            <Tooltip
                                                title="Xoá"
                                                position="top"
                                            >
                                                <span className="post-remove-button" onClick={() => this.props.deleteScheduleFilm(item.id)}><i class="fas fa-times"></i></span>
                                            </Tooltip>
                                        </td>
                                    </tr>

                                )
                            })}
                        </tbody>
                    </table>
                </div>
            </div>
        );

    }
}


export default ScheduleFilm
