import * as React from "react";

import "./edit.css";
import api from '../../../../../../services'
import imgurService from '../../../../../../services/imgur'
import CKEditor from "react-ckeditor-component";
import { scheduleAction } from '../../../../../../actions'
import { BaseModal } from '../../../../../../modals'
class Edit extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: "",
            "name": null,
            "start_time": null,
            "description": null,
            "avatar": null,
            "trailer": null,
            content: "Nội dung",
            selectedFile: null
        }
        this.update = this.update.bind(this)
        this.onChange = this.onChange.bind(this)
        this.handleFileInputChange = this.handleFileInputChange.bind(this)
        this.onFormChange = this.onFormChange.bind(this)
    }
    async handleFileInputChange(event) {
        this.setState({ selectedFile: event.target.files[0] })
    }
    onFormChange(key) {
        this.setState({ [key]: this.refs[key].value })
    }
    onChange(evt) {
        var newContent = evt.editor.getData();
        this.setState({
            description: newContent
        })
    }
    async update(event) {
        event.preventDefault();
        try {
            const body = {
                price: this.refs.price.value,
                price_member: this.refs.price_member.value
            }
            const result = await api.scheduleFilm.update(this.state.id, body)
            console.log("result: ", result)
            result.tickets = this.state.tickets
            result.room = this.state.room
            result.film = this.state.film
            this.props.dispatch(scheduleAction.update(result))
            alert("Cập nhật lịch chiếu thành công")
      
            var x = setTimeout(this.props.handleCloseEdit(), 1000)
            clearTimeout(x)
        } catch (error) {
            console.log('err: ', error)
            alert("Cập nhật không thành công")
        }
    }
    async componentDidMount() {
        const scheduleFilm = this.props.scheduleFilm.find((item) => { return item.id == this.props.id })
        this.setState(scheduleFilm)
    }
    componentWillReceiveProps(nextProps) {
        if (this.state.id !== nextProps.id) {
            return true
        }
        return false
    }

    shouldComponentUpdate(nextProps, nextState) {
        return true;
    }
    render() {
        return (
            <BaseModal show={true} handleClose={this.props.handleCloseEdit} style={{ height: "350px", width: "700px" }}>
                <div className="edit-main">
                    <form onSubmit={this.update} id="add-post-form" encType="multipart/form-data">
                        <div className="form-group">
                            <label className="input-label">Giá vé</label><br/>
                            <input
                                type="text"
                                className="form-control contact-input"
                                placeholder="Giá vé thường"
                                ref="price"
                                defaultValue={this.state.price}
                                required
                            />
                        </div>
                        <div className="form-group">
                            <label className="input-label">Giá vé thành viên</label><br/>
                            <input
                                type="text"
                                className="form-control contact-input"
                                placeholder="Giá vé thành viên"
                                ref="price_member"
                                defaultValue={this.state.price_member}
                                required
                            />
                        </div>
                       
                        <button type="submit" className="btn btn-primary" style={{ width: "150px", "margin-top": "14px" }}>
                            Cập nhật
                        </button>

                    </form>
                </div>
            </BaseModal>
        );
    }
}

export default Edit;
