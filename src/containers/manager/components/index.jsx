import General from './general/general'
import Sidebar from './sidebar/sidebar'
import Films from './films/film'
import FilmDetail from './filmDetail/filmDetail'
import ScheduleFilms from './scheduleFilms/scheduleFilms'
import Rooms from './rooms/rooms'
import Statistic from './statistic/statistic'
import RoomDetail from './roomDetail/roomDetail'

export {
    General, Sidebar, Films, FilmDetail, ScheduleFilms, Rooms, Statistic, RoomDetail
}