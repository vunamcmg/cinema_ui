import * as React from "react";
import { SelectInput } from '../../../../elements'
import './statistic.css'
import { Line } from 'react-chartjs-2';
import { AreaChart } from './components'
import api from '../../../../services'
import * as _ from 'lodash'
import DatePicker from "react-datepicker";
import * as moment from 'moment'

class Statistic extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            statisticAmountData: [],
            statisticCancelData: [],
            amountLabels: [],
            cancelLabels: [],
            isAmountNodata: false,
            isCancelNodata: false,
            status: "DAY",
            cancel: {
                start_time: moment("2017-12-09T07:38:15.668Z").format(),
                end_time: moment("2019-12-15T07:38:15.668Z").format(),
                status: "DAY"
            },
            amount: {
                start_time: moment("2017-12-09T07:38:15.668Z").format(),
                end_time: moment("2019-12-15T07:38:15.668Z").format(),
                status: "DAY"
            }

        }
        this.refreshStatistic = this.refreshStatistic.bind(this)
        this.handleStartAmountChange = this.handleStartAmountChange.bind(this)
        this.handleEndAmountChange = this.handleEndAmountChange.bind(this)
        this.handleSearchStatusAmountChange = this.handleSearchStatusAmountChange.bind(this)
        this.handleStartCancelChange = this.handleStartCancelChange.bind(this)
        this.handleEndCancelChange = this.handleEndCancelChange.bind(this)
        this.handleSearchStatusCancelChange = this.handleSearchStatusCancelChange.bind(this)
    }
    async handleSearchStatusAmountChange(field, value) {
        this.state.amount.status = value
        await this.setState({
            amount: this.state.amount
        })
        this.refreshStatistic()

    }
    async handleStartAmountChange(date) {
        this.state.amount.start_time = moment(date).format()
        await this.setState({
            amount: this.state.amount
        });
        this.refreshStatistic()
    }

    async handleEndAmountChange(date) {
        this.state.amount.end_time = moment(date).format()
        await this.setState({
            amount: this.state.amount
        });
        this.refreshStatistic()
    }
    async handleSearchStatusCancelChange(field, value) {
        this.state.cancel.status = value
        await this.setState({
            cancel: this.state.cancel
        })
        this.refreshStatistic()

    }
    async handleStartCancelChange(date) {
        this.state.cancel.end_time = moment(date).format()
        await this.setState({
            cancel: this.state.cancel
        });
        this.refreshStatistic()
    }

    async handleEndCancelChange(date) {
        this.state.cancel.end_time = moment(date).format()
        await this.setState({
            cancel: this.state.cancel
        });
        this.refreshStatistic()
    }
    async refreshStatistic() {
        try {

            const statisticAmountData = await api.scheduleFilm.statisticTicket({
                type: this.state.amount.status,
                start_time: moment(this.state.amount.start_time).toDate() || "2017-12-09T07:38:15.668Z",
                end_time: moment(this.state.amount.end_time).toDate() || "2019-12-15T07:38:15.668Z"
            })
            const statisticCancelData = await api.scheduleFilm.statisticCancel({
                type: this.state.cancel.status,
                start_time: moment(this.state.cancel.start_time).toDate() || "2017-12-09T07:38:15.668Z",
                end_time: moment(this.state.cancel.end_time).toDate() || "2019-12-15T07:38:15.668Z"
            })
            if (!statisticAmountData) {
                this.setState({ isAmountNodata: true })
            } else {
                const { statisticData, labels } = await this.convertRawDataToStatisticData(this.state.amount.status, statisticAmountData)
                this.setState({
                    amountLabels: labels || [],
                    statisticAmountData: statisticData || [],
                    isAmountNodata: false
                })
            }
            if (!statisticCancelData) {
                this.setState({ isCancelNodata: true })
            } else {
                const { statisticData, labels } = await this.convertRawDataToStatisticData(this.state.cancel.status, statisticCancelData, true)
                this.setState({
                    cancelLabels: labels || [],
                    statisticCancelData: statisticData || [],
                    isCancelNodata: false
                })
            }
            this.forceUpdate()
        } catch (err) {
            console.log("err", err)
        }
    }
    async componentDidMount() {
        await this.setState({
            status: "DAY",
            start_time: moment("2017-12-09T07:38:15.668Z").format(),
            end_time: moment("2019-12-15T07:38:15.668Z").format()
        })
        this.refreshStatistic()

    }
    async convertRawDataToStatisticData(status, rawData, isPercent = false) {
        let data = {}
        let labels = []
        let isFirst = true
        for (const item in rawData) {
            for (const film of rawData[item]) {
                let date = ""
                switch (status) {
                    case "DAY":
                        date = `${film.day}/${film.month}/${film.year}`.toString()
                        if (isFirst) {
                            labels.push(`${film.day - 1}/${film.month}/${film.year}`.toString())
                            isFirst = false
                        }
                        break
                    case "WEEK":
                        date = `${film.week}/${film.year}`.toString()
                        if (isFirst) {
                            labels.push(`${film.week - 1}/${film.year}`.toString())
                            isFirst = false
                        }
                        break
                    case "MONTH":
                        date = `${film.month}/${film.year}`.toString()
                        if (isFirst) {
                            labels.push(`${film.month - 1}/${film.year}`.toString())
                            isFirst = false
                        }
                        break
                    case "QUARTER":
                        date = `${film.quarter}/${film.year}`.toString()
                        if (isFirst) {
                            labels.push(`${film.quarter - 1}/${film.year}`.toString())
                            isFirst = false
                        }
                        break
                    case "YEAR":
                        date = `${film.year}`.toString()
                        if (isFirst) {
                            labels.push(`${film.year - 1}`.toString())
                            isFirst = false
                        }
                        break
                }
                labels.push(date)
                if (isPercent) {
                    data[film.name] = data[film.name] !== undefined ? data[film.name].concat([{ count: film.value, date: date }]) : [{ count: film.value, date: date }]
                } else {
                    data[film.name] = data[film.name] !== undefined ? data[film.name].concat([{ count: film.count, date: date }]) : [{ count: film.count, date: date }]
                }

            }
        }
        let statisticData = []
        labels = _.union(_.sortBy(labels))
        for (const filmName in data) {
            const newFilmData = new Array(labels.length).fill("0")
            for (const element of data[filmName]) {
                const date = element.date
                if (labels.indexOf(date) !== -1) {
                    if (isPercent) {
                        newFilmData[labels.indexOf(date)] = (Number(element.count) * 100).toString()
                    } else {
                        newFilmData[labels.indexOf(date)] = element.count
                    }

                }
            }
            statisticData.push({
                name: filmName,
                data: newFilmData
            })
        }
        return {
            statisticData, labels
        }
    }
    render() {
        const amountStatus = [
            {
                id: "DAY",
                name: "Ngày"
            },
            {
                id: "MONTH",
                name: "Tháng"
            },
            {
                id: "YEAR",
                name: "Năm"
            }
        ]
        const cancelStatus = [
            {
                id: "DAY",
                name: "Ngày"
            },
            {
                id: "WEEK",
                name: "Tuần"
            },
            {
                id: "MONTH",
                name: "Tháng"
            },
            {
                id: "QUARTER",
                name: "Quý"
            }
        ]
        return (
            <div className="posts">
                <b>Tỉ lệ mua vé theo phim</b>
                
                <div className="query">

                    <div className="form-group date-picker" >
                        <label>Ngày bắt đầu</label>
                        <br />
                        <DatePicker
                            className="contact-input"
                            selected={this.state.amount.start_time}
                            onChange={this.handleAmountStartChange}
                            showTimeSelect
                            timeFormat="HH:mm"
                            timeIntervals={15}
                            dateFormat="MMMM d, yyyy h:mm aa"
                            timeCaption="time"
                        />
                    </div>
                    <div className="form-group date-picker" >
                        <label>Ngày kết thúc</label>
                        <br />
                        <DatePicker
                            className="contact-input"
                            selected={this.state.amount.end_time}
                            onChange={this.handleAmountEndChange}
                            showTimeSelect
                            timeFormat="HH:mm"
                            timeIntervals={15}
                            dateFormat="MMMM d, yyyy h:mm aa"
                            timeCaption="time"
                        />
                    </div>
                    <div>
                        <SelectInput label="Thống kê theo" refKey="type" data={amountStatus} titleKey="name" valueKey="id" handleInputChange={this.handleSearchStatusAmountChange} />
                    </div>
                </div>
                {this.state.isAmountNodata ? <h2>Không có dữ liệu</h2> :
                    <AreaChart labels={this.state.amountLabels} statisticData={this.state.statisticAmountData} height="300" id="1" />
                }
                <hr />
                <b>Tỉ lệ huỷ vé theo phim</b>
                <hr />
                <div className="query">

                    <div className="form-group date-picker" >
                        <label>Ngày bắt đầu</label>
                        <br />
                        <DatePicker
                            className="contact-input"
                            selected={this.state.cancel.start_time}
                            onChange={this.handleStartCancelChange}
                            showTimeSelect
                            timeFormat="HH:mm"
                            timeIntervals={15}
                            dateFormat="MMMM d, yyyy h:mm aa"
                            timeCaption="time"
                        />
                    </div>
                    <div className="form-group date-picker" >
                        <label>Ngày kết thúc</label>
                        <br />
                        <DatePicker
                            className="contact-input"
                            selected={this.state.cancel.end_time}
                            onChange={this.handleEndCancelChange}
                            showTimeSelect
                            timeFormat="HH:mm"
                            timeIntervals={15}
                            dateFormat="MMMM d, yyyy h:mm aa"
                            timeCaption="time"
                        />
                    </div>
                    <div>
                        <SelectInput label="Thống kê theo" refKey="type" data={cancelStatus} titleKey="name" valueKey="id" handleInputChange={this.handleSearchStatusCancelChange} />
                    </div>
                </div>
                {this.state.isCancelNodata ? <h2>Không có dữ liệu</h2> :
                    <AreaChart labels={this.state.cancelLabels} statisticData={this.state.statisticCancelData} height="300" id="1" />
                }
            </div>
        );
    }
}


export default Statistic
