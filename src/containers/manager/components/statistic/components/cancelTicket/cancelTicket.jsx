import * as React from "react";

import './statistic.css'
import { Line } from 'react-chartjs-2';

class Statistic extends React.Component {

    constructor(props) {
        super(props);
    }
    async componentDidMount() {

    }
    render() {
        const data = {
            labels: ['January', 'February', 'March'],
            datasets: [{
                backgroundColor: '#BF360C',
                borderColor: '#BF360C',
                data: [10,50,80],
                label: 'Avatar'
            }, {
                backgroundColor: '#E65100',
                borderColor: '#E65100',
                data: [70,20,10],
                label: 'Xạ thủ',
                fill: '-1'
            }, {
                backgroundColor: '#0091EA',
                borderColor: '#0091EA',
                data: [20,30,10],
                label: 'Diệp Vấn',
                fill: 1
            }]
        };
        var options = {
            maintainAspectRatio: false,
            spanGaps: false,
            elements: {
                line: {
                    tension: 0.000001
                }
            },
            scales: {
                yAxes: [{
                    stacked: true
                }]
            },
            plugins: {
                filler: {
                    propagate: false
                },
                'samples-filler-analyser': {
                    target: 'chart-analyser'
                }
            }
        };

        return (
            <div>
                <b>Tỉ lệ mua vé theo phim</b>
                <hr/>
                <Line data={data} options={options} height="300" id="1"/>
                {/* <b>Tỉ lệ huỷ vé theo phim</b>
                <hr/>
                <Line data={data} options={options} height="300" id="2"/> */}
            </div>
        );
    }
}


export default Statistic
