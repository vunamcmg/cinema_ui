import * as React from "react";
import { Route, Link, Router } from "react-router-dom";
import api from '../../../../services/index'

import { EditFilm, AddFilm } from './components'

import * as _ from 'lodash'
import "./film.css";
import * as moment from 'moment'

import {
    Tooltip,
} from 'react-tippy';

import { filmAction } from '../../../../actions'
class Films extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            posts: [],
            isShowAddPost: false,
            content: 'Nội dung của bạn',
            selectedFile: null,
            showEdit: false,
            showAdd: false,
            selectedFilmId: null
        };
        this.edit = this.edit.bind(this)
        this.open = this.open.bind(this)
        this.addFilm = this.addFilm.bind(this)
        this.delete = this.delete.bind(this)
        this.handleCloseAdd = this.handleCloseAdd.bind(this)
        this.handleCloseEdit = this.handleCloseEdit.bind(this)
    }
    async delete(id) {
        this.props.deleteFilm(id)
    }
    async open(id) {
        //this.setState({ selectedFilmId: id })
        this.props.openFilmDetail(id)
        // window.open(trailer, "_blank")
    }
    async edit(id) {
        console.log("id phim: ", id)
        this.setState({ showEdit: true, selectedFilmId: id })
    }
    async addFilm() {
        this.setState({ showAdd: true })
    }
    handleCloseAdd() {
        this.setState({ showAdd: false })
    }
    handleCloseEdit() {
        this.setState({ showEdit: false })
    }


    componentWillReceiveProps(nextProps) {
        return true
    }

    shouldComponentUpdate(nextProps, nextState) {
        return true;
    }

    render() {
        const { film } = this.props
        return (

            <div className="add-post-main">

                {this.state.showEdit ? <EditFilm id={this.state.selectedFilmId} handleCloseEdit={this.handleCloseEdit} {...this.props} /> : null}
                {this.state.showAdd ? <AddFilm handleCloseAdd={this.handleCloseAdd} {...this.props} /> : null}
                <div className="film-toolbar">
                    <button className="film-toolbar-button" onClick={this.addFilm}>Thêm phim</button>
                    {/* <button className="film-toolbar-button" onClick={this.addFilm}>Thống kê</button> */}
                </div>
                <br />
                {this.state.isShowAddPost ? <div>
                    <form onSubmit={this.addPost} id="add-post-form" encType="multipart/form-data">
                        <div className="form-group">
                            <input
                                type="text"
                                className="form-control contact-input"
                                placeholder="Tiêu đề"
                                ref="title"
                                required
                            />
                        </div>
                        <div className="form-group">
                            <input
                                type="text"
                                className="form-control contact-input"
                                placeholder="Tác giả"
                                ref="author"
                                required
                            />
                        </div>
                        <div className="form-group">
                            <input
                                type="file"
                                className="form-control contact-input"
                                placeholder="Thumbnail"
                                ref="thumb"
                                required
                                onChange={this.handleFileInputChange}
                            />
                        </div>
                        {/* <CKEditor
                            activeClass="p10"
                            content={this.state.content}
                            events={{
                                "blur": this.onBlur,
                                "afterPaste": this.afterPaste,
                                "change": this.onChange
                            }}
                        /> */}

                        <button type="submit" className="btn btn-primary">
                            Đăng bài
                            </button>

                    </form>
                </div> : null}
                <div className="posts">
                    <table className="admin-posts-table">
                        <tbody>
                            <tr>
                                <th>Thứ tự</th>
                                <th>Poster</th>
                                <th>Tên phim</th>
                                <th>Tình trạng</th>
                                <th>Ngày khởi chiếu</th>
                                <th>Thao tác</th>
                            </tr>
                            {film.map((item, index) => {
                                let status
                                switch(item.status){
                                    case "WILL_BE_RELEASE":
                                        status = "Sắp công chiếu"
                                        break
                                    case "RELEASING":
                                        status = "Đang công chiếu"
                                        break
                                    case "RELEASED":
                                        status = "Đã công chiếu"
                                        break
                                    default:
                                        status = "Đang cập nhật"
                                }
                                return (
                                    <tr key={item.id}>
                                        <td>{index + 1}</td>
                                        <td ><img src={item.avatar} className="img-response avatar-image"></img></td>
                                        <td>{item.name}</td>
                                        <td>{status}</td>
                                        <td>{moment(item.start_time).format('DD/MM/YYYY')}</td>
                                        {/* <td>Xoá </td> */}
                                        <td className="action-td">
                                            <Tooltip
                                                title="Chi tiết"
                                                position="top"
                                            >
                                                <span className="post-open-button" onClick={() => this.open(item.id)}><i class="fas fa-share-square"></i></span>
                                            </Tooltip>
                                            <Tooltip
                                                title="Chỉnh sửa"
                                                position="top"
                                            >
                                                <span className="post-edit-button" onClick={() => this.edit(item.id)}> <i class="fas fa-pen"></i> </span>
                                            </Tooltip>
                                            <Tooltip
                                                title="Xoá"
                                                position="top"
                                            >
                                                <span className="post-remove-button" onClick={() => this.delete(item.id)}><i class="fas fa-times"></i></span>
                                            </Tooltip>
                                        </td>
                                    </tr>

                                )
                            })}
                        </tbody>
                    </table>
                </div>
            </div>
        );

    }
}


export default Films
