import EditFilm from './edit/edit'
import AddFilm from './add/add'

export {
    EditFilm, AddFilm
}