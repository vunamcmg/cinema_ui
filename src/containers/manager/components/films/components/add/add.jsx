import * as React from "react";

import "./add.css";
import api from '../../../../../../services'
import imgurService from '../../../../../../services/imgur'
import CKEditor from "react-ckeditor-component";
import { filmAction } from '../../../../../../actions'
import { BaseModal } from '../../../../../../modals'
import DatePicker from "react-datepicker";
import { TextInput, SelectInput } from '../../../../../../elements'
import * as moment from 'moment'
class AddFilm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: "",
            "name": null,
            "start_time": moment().format(),
            "description": null,
            "avatar": null,
            "trailer": null,
            content: "Nội dung",
            selectedFile: null
        }
        this.add = this.add.bind(this)
        this.onChange = this.onChange.bind(this)
        this.handleFileInputChange = this.handleFileInputChange.bind(this)
        this.onFormChange = this.onFormChange.bind(this)
        this.handleChange = this.handleChange.bind(this)
        this.handleInputChange = this.handleInputChange.bind(this)
    }

    async handleInputChange(field, value) {
        this.setState({ [field]: value })
    }

    handleChange(date) {
        this.setState({
            start_time: moment(date).format()
        });
    }
    async handleFileInputChange(event) {
        this.setState({ selectedFile: event.target.files[0] })
    }
    onFormChange(key) {
        this.setState({ [key]: this.refs[key].value })
    }
    onChange(evt) {
        var newContent = evt.editor.getData();
        this.setState({
            description: newContent
        })
    }
    async add(event) {
        event.preventDefault();
        try {
            let link = this.state.avatar
            if (this.state.selectedFile !== null) {
                link = await imgurService.uploadImage(this.state.selectedFile)
            }
            const body = {
                name: this.refs.name.value,
                avatar: link,
                description: this.state.description,
                start_time: this.state.start_time,
                trailer: this.refs.trailer.value,
                avatar: this.refs.avatar.value,
                status: this.state.status
            }
            const result = await api.film.add(body)
            this.props.dispatch(filmAction.unshift({
                id: result.id,
                name: this.refs.name.value,
                description: this.state.description,
                start_time: this.state.start_time,
                trailer: this.refs.trailer.value,
                avatar: this.refs.avatar.value,
                status: this.state.status
            }))
            alert("Thêm phim thành công")
            this.props.handleCloseAdd()
        } catch (error) {
            alert("Cập nhật không thành công")
        }
    }
    async componentDidMount() {
        //  const film = this.props.film.find((item) => { return item.id == this.props.id })
        // this.setState(film)
    }
    componentWillReceiveProps(nextProps) {
        if (this.state.id !== nextProps.id) {
            return true
        }
        return false
    }

    shouldComponentUpdate(nextProps, nextState) {
        return true;
    }
    render() {
        const filmStatus = [
            {
                id: "WILL_BE_RELEASE",
                name: "Sắp chiếu"
            },
            {
                id: "RELEASING",
                name: "Đang chiếu"
            },
            {
                id: "RELEASED",
                name: "Dừng công chiếu"
            }
        ]
        return (
            <BaseModal show={true} handleClose={this.props.handleCloseAdd} style={{ height: "650px", width: "700px" }}>
                <div className="edit-main">
                    <form onSubmit={this.add} id="add-post-form" encType="multipart/form-data">
                        <div className="form-group">
                            <input
                                type="text"
                                className="form-control contact-input"
                                placeholder="Tên phim"
                                ref="name"
                                defaultValue={this.state.name}
                                required
                            />
                        </div>
                        <div className="form-group" >
                            <label>Ngày khởi chiếu: </label>
                            {/* <br/> */}
                            <DatePicker
                                className="contact-input"
                                selected={this.state.start_time}
                                onChange={this.handleChange}
                                showTimeSelect
                                timeFormat="HH:mm"
                                timeIntervals={15}
                                dateFormat="MMMM d, yyyy h:mm aa"
                                timeCaption="time"
                            />
                        </div>
                        <div className="row">
                            <div className="col-md-6">
                                <div className="form-group">
                                    <input
                                        type="text"
                                        className="form-control contact-input"
                                        placeholder="Trailer"
                                        ref="trailer"
                                        defaultValue={this.state.trailer}
                                        required
                                    />
                                </div>
                            </div>
                            <div className="col-md-6">

                                <div className="form-group">
                                    <input
                                        type="text"
                                        className="form-control contact-input"
                                        placeholder="Avatar"
                                        ref="avatar"
                                        defaultValue={this.state.avatar}
                                        required
                                    />
                                </div></div>
                        </div>



                        <SelectInput refKey="status" data={filmStatus} titleKey="name" valueKey="id" handleInputChange={this.handleInputChange} />

                        <CKEditor
                            activeClass="p10"
                            content={this.state.description}
                            events={{
                                "blur": this.onBlur,
                                "afterPaste": this.afterPaste,
                                "change": this.onChange
                            }}
                            className="edit-content"
                        />
                        <button type="submit" className="btn btn-primary" style={{ width: "150px", "margin-top": "14px" }}>
                            Cập nhật
                        </button>

                    </form>
                </div>
            </BaseModal>
        );
    }
}

export default AddFilm;
