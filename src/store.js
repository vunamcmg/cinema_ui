import { createStore, applyMiddleware } from "redux";
import { createLogger } from "redux-logger";
import thunk from "redux-thunk";

import AppReducer from "./reducer";

const logger = createLogger();

const error = (store) => (next) => (action) => {
  try {
    next(action);
  } catch (error) {
    console.log("ERROR: ", error);
  }
};

const createAppStore = applyMiddleware(logger, error, thunk)(createStore);

export function configureStore(initialState) {
  return createAppStore(AppReducer, initialState);
}
