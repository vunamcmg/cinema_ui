import { combineReducers } from "redux";
import * as guid from "guid";
import * as _ from "lodash";


import bill from './bill'
import billItem from './billItem'
import customer from './customer'
import employee from './employee'
import film from './film'
import room from './room'
import scheduleFilm from './scheduleFilm'
import seat from './seat'
import ticket from './ticket'
import user from './user'

const store = combineReducers({
  bill, billItem, customer, employee, film, room, scheduleFilm, seat, ticket, user
});

export default store;
