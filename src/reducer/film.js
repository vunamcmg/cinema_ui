import * as _ from "lodash";

let film = (state = [], action) => {
  let index = -1
  let name = "FILM"
  switch (action.type) {
    case `FETCH_${name}`:
      state = action.payload
      break;
    case `ADD_${name}`:
    case `GET_${name}`:
      state = state.concat([action.payload])
      break;
    case `UNSHIFT_${name}`:
      state.unshift(action.payload)
      break;
    case `DELETE_${name}`:
      index = _.findIndex(
        state, item => item.id == action.payload
      )
      if (index !== -1) {
        state.splice(index, 1)
      }
      break;
    case `EDIT_${name}`:
      index = _.findIndex(
        state, item => item.id == action.payload.id
      )
      if (index !== -1) {
        state[index] = action.payload
      }
      break;
    case `SEARCH_${name}`:
      state = state.filter(item => { return item.name.toLowerCase().indexOf(action.payload) !== -1 })
  }
  return state;
};

export default film
