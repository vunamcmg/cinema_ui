import * as _ from "lodash";

let scheduleFilm = (state = [], action) => {
  let index = -1
  let name = "SCHEDULEFILM"
  switch (action.type) {
    case `FETCH_${name}`:
      state = action.payload
      break;
    case `ADD_${name}`:
    case `GET_${name}`:
      state = state.concat([action.payload])
      break;
    case `UNSHIFT_${name}`:
      state.unshift(action.payload)
      break;

    case `DELETE_${name}`:
    console.log("pay load: ", action.payload)
      index = _.findIndex(
        state, item => item.id == action.payload
      )
      console.log("index: ", index)
      if (index !== -1) {
        state.splice(index, 1)
      }
      break;
    case `EDIT_${name}`:
      index = _.findIndex(
        state, item => item.id == action.payload.id
      )
      if (index !== -1) {
        state[index] = action.payload
      }
      break;
  }
  return state;
};

export default scheduleFilm