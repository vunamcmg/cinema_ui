import TextInput from './inputs/textinput/textinput'
import FileInput from './inputs/fileinput/fileinput'
import SelectInput from './inputs/selectInput/selectInput'

export {
    TextInput,
    FileInput,
    SelectInput
}