import * as React from "react";

import "./textinput.css";
class TextInput extends React.Component {
    constructor(props) {
        super(props)
        this.state = {

        }
        this.handleInputChange = this.handleInputChange.bind(this)
    }
    async handleInputChange() {
        this.props.handleInputChange(this.props.refKey, this.refs[this.props.refKey].value)
    }
    render() {
        const { label, refKey, placeholder, style, value, required = false, readOnly = false } = this.props
        return (
            <div className="form-group" style={style}>
                <label className="input-label">{label}</label>
                <br />
                <input
                    type="text"
                    className="form-control contact-input"
                    placeholder={placeholder}
                    ref={refKey}
                    defaultValue={value}
                    onChange={this.handleInputChange}
                    required={required}
                    readOnly={readOnly}
                />
            </div>
        )
    }
}

export default TextInput