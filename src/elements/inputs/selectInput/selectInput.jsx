import * as React from "react";
import moment from 'moment';

import "./selectInput.css";
class SelectInput extends React.Component {
    constructor(props) {
        super(props)
        this.state = {

        }
        this.handleInputChange = this.handleInputChange.bind(this)
    }
    async handleInputChange(e) {
        this.props.handleInputChange(this.props.refKey, e.target.value)
    }
    render() {
        const { label, refKey, style, value = this.props.data[0][valueKey], data, titleKey, valueKey, required = false, handleInputChange, time = false } = this.props

        return (
            <div className="form-group">
                {label ? <div><label >{label}</label>
                    <br /></div> : null}
                <select ref={refKey} className="select-input" onChange={this.handleInputChange}>
                    {data.map(item => {
                        if (time)
                            return <option key={item[titleKey]} value={item[valueKey]} selected={value === item[valueKey]}>{moment(item[titleKey]).utc().format("DD/MM/YYYY HH:mm")}</option>
                        return <option key={item[titleKey]} value={item[valueKey]} selected={value === item[valueKey]}>{item[titleKey]}</option>

                    })}
                </select>
            </div>
        )
    }
}

export default SelectInput