import * as React from "react";

import "./fileinput.css";
class FileInput extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            selectedFile: null
        }
        this.handleFileInputChange = this.handleFileInputChange.bind(this)
    }
    async handleFileInputChange(event) {
        this.setState({ selectedFile: event.target.files[0] })
    }
    render() {
        const { label, ref, placeholder, style, value, required = false } = this.props
        return (
            <div className="form-group" style={style}>
                <label>{label}</label>
                <br />
                <input
                    type="file"
                    className="thumb-select"
                    placeholder="Thumbnail"
                    defaultValue={value}
                    ref="thumb"
                    onChange={this.handleFileInputChange}
                />
            </div>
        )
    }
}

export default FileInput