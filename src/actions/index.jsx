import { BaseAction } from './base'
import { EmployeeAction } from './employee'
import { FilmAction } from './film'
import { RoomAction } from './room'
import { ScheduleAction} from './scheduleFilm'
import { SeatAction} from './seat'
import { CustomerAction } from './customer'
import { BillAction } from './bill'
import { BillItemAction } from './billItem'
import { TicketAction } from './ticket'
import { UserAction } from './user'

const employeeAction = new EmployeeAction()
const filmAction = new FilmAction()
const roomAction = new RoomAction()
const scheduleAction = new ScheduleAction()
const seatAction = new SeatAction()
const customerAction = new CustomerAction()
const billAction = new BillAction()
const billItemAction = new BillItemAction()
const ticketAction = new TicketAction()
const userAction = new UserAction()

export {
    employeeAction, 
    filmAction, 
    roomAction,
    scheduleAction,
    seatAction,
    customerAction,
    billAction,
    billItemAction,
    ticketAction,
    userAction
}
