import BaseAction from './base'

export class FilmAction extends BaseAction {
    constructor(){
        super("film")
    }
    search(query) {
        return {
            type: `SEARCH_${this.name}`,
            payload: query
        }
    }
}