
import * as React from "react";
import { render } from "react-dom";
import { configureStore } from "./store";
import { Provider } from "react-redux";
import registerServiceWorker from './registerServiceWorker';

import { Home, Booking, Manager, Login } from "./containers/index";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import 'react-tippy/dist/tippy.css';
import "react-datepicker/dist/react-datepicker.css";

const styles = {
  fontFamily: "sans-serif",
  textAlign: "center"
};
const appStore = configureStore();

render(
  <div>
    <Provider store={appStore}>
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={Home} key="home" />
          <Route exact path="/manager" component={Manager} key="manager" />
          <Route exact path="/booking" component={Booking} key="booking" />
          <Route exact path="/login" component={Login} key="login" />
        </Switch>
      </BrowserRouter>
    </Provider>
  </div>,
  document.getElementById("root")
);
registerServiceWorker();
