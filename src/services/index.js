import { EmployeeService } from './crud/employee'
import { FilmService } from './crud/film'
import { RoomService } from './crud/room'
import { ScheduleFilmService } from './crud/scheduleFilm'
import { SeatService } from './crud/seat'
import { CustomerService } from './crud/customer'
import { BillService } from './crud/bill'
import { BillITemService }  from './crud/billItem'
import { TicketService } from './crud/ticket'

class Api {
    constructor(){

    }
    employee = new EmployeeService()
    film = new FilmService()
    room = new RoomService()
    scheduleFilm = new ScheduleFilmService()
    seat = new SeatService()
    customer = new CustomerService()
    bill = new BillService()
    billItem = new BillITemService()
    ticket = new TicketService()
}
const api = new Api

export default api
