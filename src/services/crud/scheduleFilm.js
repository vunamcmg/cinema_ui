import * as Request from 'request-promise'
import { environment } from '../../environments/index.jsx'
import { CrudService  } from '../crud'
import * as _ from 'lodash'


export class ScheduleFilmService extends CrudService {
    constructor(){
        super("schedule_film")
    }
    async statisticTicket(body, option = {}) {
        const options = {
            uri: this.baseUrl('report/ticket'),
            method: "POST",
            qs: this._paserQuery(option.query),
            headers: _.merge({
                'User-Agent': 'Request-Promise',
                'Content-Type':"Application/json"
            }, option.headers),
            body: body,
            json: true
        }
        const res = await this.exec(options)
        return res.result
    }
    async statisticCancel(body, option = {}) {
        const options = {
            uri: this.baseUrl('report/ticket/cancel'),
            method: "POST",
            qs: this._paserQuery(option.query),
            headers: _.merge({
                'User-Agent': 'Request-Promise',
                'Content-Type':"Application/json"
            }, option.headers),
            body: body,
            json: true
        }
        const res = await this.exec(options)
        return res.result
    }
}