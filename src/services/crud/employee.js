import * as Request from 'request-promise'
import { environment } from '../../environments/index.jsx'
import { CrudService  } from '../crud'

export class EmployeeService extends CrudService {
    constructor(){
        super("employee")
    }

}