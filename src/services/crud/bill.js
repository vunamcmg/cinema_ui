import * as Request from 'request-promise'
import { environment } from '../../environments/index.jsx'
import { CrudService  } from '../crud'
import * as _ from 'lodash'

export class BillService extends CrudService {
    constructor(){
        super("bill")
    }
    async buyTicket(body, option = {}) {
        const options = {
            uri: this.baseUrl('buyticket'),
            method: "POST",
            qs: this._paserQuery(option.query),
            headers: _.merge({
                'User-Agent': 'Request-Promise',
                'Content-Type':"Application/json"
            }, option.headers),
            body: body,
            json: true
        }
        const res = await this.exec(options)
        return res.result
    }
    async order(body, option = {}) {
        const options = {
            uri: this.baseUrl('order'),
            method: "POST",
            qs: this._paserQuery(option.query),
            headers: _.merge({
                'User-Agent': 'Request-Promise',
                'Content-Type':"Application/json"
            }, option.headers),
            body: body,
            json: true
        }
        const res = await this.exec(options)
        return res.result
    }
    async changeTicket(body, option = {}) {
        const options = {
            uri: this.baseUrl('order'),
            method: "PUT",
            qs: this._paserQuery(option.query),
            headers: _.merge({
                'User-Agent': 'Request-Promise',
                'Content-Type':"Application/json"
            }, option.headers),
            body: body,
            json: true
        }
        const res = await this.exec(options)
        return res.result
    }
    async paybill(id, option = {}) {
        const options = {
            uri: this.baseUrl('updatestatusbill'),
            method: "POST",
            qs: this._paserQuery(option.query),
            headers: _.merge({
                'User-Agent': 'Request-Promise',
                'Content-Type':"Application/json"
            }, option.headers),
            body: {
                bill_id: id
            },
            json: true
        }
        const res = await this.exec(options)
        return res.result
    } 
    async cancelTicket(id, option = {}) {
        const options = {
            uri: this.baseUrl('cancelBill'),
            method: "POST",
            qs: this._paserQuery(option.query),
            headers: _.merge({
                'User-Agent': 'Request-Promise',
                'Content-Type':"Application/json"
            }, option.headers),
            body: {
                bill_id: id
            },
            json: true
        }
        const res = await this.exec(options)
        return res.result
    }
}