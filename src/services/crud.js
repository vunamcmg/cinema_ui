import * as Request from 'request-promise'
import { environment } from '../environments/index'

import * as _ from 'lodash'
import * as queryString from 'querystring'
export class CrudService {
    constructor(subpath) {
        this.subpath = subpath
    }
    baseUrl(path = '') {
        return `${environment.production.host}/api/${this.subpath}/${path}`
    }
    async exec(options) {
        try {
            return await Request(options)
        } catch (err) {
            console.log("err: ", err)
            throw err
        }
    }
    async getList(option = {}) {
        const options = {
            uri: this.baseUrl(),
            method: "GET",
            qs: this._paserQuery(option.query),
            headers: _.merge({
                'User-Agent': 'Request-Promise',
                'Content-Type':"Application/json"
            }, option.headers),
            json: true
        }
        const res = await this.exec(options)
        return res.results.objects.records
    }
    async getItem(id, option = {}) {
        const options = {
            uri: this.baseUrl(id),
            method: "GET",
            qs: this._paserQuery(option.query),
            headers: _.merge({
                'User-Agent': 'Request-Promise',
                'Content-Type':"Application/json"
            }, option.headers),
            json: true
        }
        const res = await this.exec(options)
        return res.result
    }
    async delete(id, option = {}) {
        const options = {
            uri: this.baseUrl(id),
            method: "DELETE",
            qs: this._paserQuery(option.query),
            headers: _.merge({
                'User-Agent': 'Request-Promise',
                'Content-Type':"Application/json"
            }, option.headers),
            json: true
        }
        const res = await this.exec(options)
        return res.result
    }
    async update(id, body, option = {}) {
        const options = {
            uri: this.baseUrl(id),
            method: "PUT",
            qs: this._paserQuery(option.query),
            headers: _.merge({
                'User-Agent': 'Request-Promise',
                'Content-Type':"Application/json"
            }, option.headers),
            body: body,
            json: true
        }
        const res = await this.exec(options)
        return res.result
    }
    async add(body, option = {}) {
        const options = {
            uri: this.baseUrl(),
            method: "POST",
            qs: this._paserQuery(option.query),
            headers: _.merge({
                'User-Agent': 'Request-Promise',
                'Content-Type':"Application/json"
            }, option.headers),
            body: body,
            json: true
        }
        const res = await this.exec(options)
        return res.result
    }
    _paserQuery(query = {}) {
        let parsedQuery = _.merge({}, query)
        if (query.filter) {
            parsedQuery.filter = JSON.stringify(query.filter);
        }
        if (query.order) {
            parsedQuery.order = JSON.stringify(query.order);
        }
        if (query.scopes) {
            parsedQuery.scopes = JSON.stringify(query.scopes);
        }
        if (query.fields) {
            parsedQuery.fields = JSON.stringify(query.fields);
        }
        if (query.items) {
            parsedQuery.items = JSON.stringify(query.items);
        }
        if (query.populates) {
            parsedQuery.populates = JSON.stringify(query.populates)
        }
        if(query.limit) {
            parsedQuery.limit = JSON.stringify(query.limit)
        }
        if(query.offset){
            parsedQuery.offset = JSON.stringify(query.offset)
        }
        return parsedQuery;
    }

}