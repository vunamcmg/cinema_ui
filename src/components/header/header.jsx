import * as React from "react";
import { Link } from "react-router-dom";

import "./header.css";

class Header extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div className="header">
        {/* <div className="bottom-header">
          <p>
            {" "}
            <i className="fa fa-address-book-o"> </i> Hotline: 0979 614 516{" "}
          </p>
        </div> */}
        {/* <hr className="header-line" /> */}
        <ul className="nav">
          <li className="nav-item nav-brand">LotteCinema</li>
          <li className="nav-item ">
            <Link to="/" className="nav-link">
              Trang chủ{" "}
            </Link>
          </li>
          <li className="nav-item">
            <Link to="/dichvu" className="nav-link">
              Dịch vụ{" "}
            </Link>
          </li>
          <li className="nav-item">
            <Link to="/category" className="nav-link">
              Bài viết{" "}
            </Link>
          </li>
          <li className="nav-item">
            <a className="nav-link" href="#">
              Tài khoản
            </a>
          </li>
        </ul>
        {/* <hr className="header-line" /> */}
      </div>
    );
  }
}

export default Header;
