import * as React from "react";
import { Link } from "react-router-dom";

import "./footer.css";

class Footer extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div className="footer">
        <div className="container">
          <div className="row footer-row">
            <div className="col-lg-3 col-md-3 footer-element">
              <h3>LotteCinema </h3>
              <p>
                {" "}
                Lorem ipsum dolor sit amet, consectetur ad ac lacinia arcu
                auctor.
              </p>
            </div>
            <div className="col-lg-3 col-md-3 footer-element">
              <h3>Hệ thống bán vé</h3>
              <p>
                {" "}
                Hệ thống bán vé xem phim hiện đại nhất Việt Nam
              </p>
            </div>
            <div className="col-lg-3 col-md-3 footer-element">
              <h3>Đối tác </h3>
              <p>
                {" "}
                Paramount Picture <br />
                20 Century <br />
                Disney <br />
              </p>
            </div>
            <div className="col-lg-3 col-md-3 footer-element">
              <h3>Bảo trợ thông tin </h3>
              <p>
                {" "}
                Lorem ipsum dolor sit amet, consectetur ad ac lacinia arcu
                auctor.
              </p>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Footer;
